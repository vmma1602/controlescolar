﻿namespace ControlEscolar2
{
    partial class Calificaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Calificaciones));
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnExportarExcel = new System.Windows.Forms.Button();
            this.btnExportarPDF = new System.Windows.Forms.Button();
            this.gpoAlumnos = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtApm = new System.Windows.Forms.TextBox();
            this.txtApp = new System.Windows.Forms.TextBox();
            this.txtNombreAlumno = new System.Windows.Forms.TextBox();
            this.cmbAlumnos = new System.Windows.Forms.ComboBox();
            this.gpoGrupos = new System.Windows.Forms.GroupBox();
            this.cmbGrupo = new System.Windows.Forms.ComboBox();
            this.gpoMateria = new System.Windows.Forms.GroupBox();
            this.cmbMateria = new System.Windows.Forms.ComboBox();
            this.dtgCalificaciones = new System.Windows.Forms.DataGridView();
            this.txtParcial1 = new System.Windows.Forms.TextBox();
            this.txtParcial4 = new System.Windows.Forms.TextBox();
            this.txtParcial3 = new System.Windows.Forms.TextBox();
            this.txtParcial2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.gpoCalificaciones = new System.Windows.Forms.GroupBox();
            this.bgwExportar = new System.ComponentModel.BackgroundWorker();
            this.gpoAlumnos.SuspendLayout();
            this.gpoGrupos.SuspendLayout();
            this.gpoMateria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCalificaciones)).BeginInit();
            this.gpoCalificaciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar.BackgroundImage")));
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminar.Location = new System.Drawing.Point(439, 454);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(58, 57);
            this.btnEliminar.TabIndex = 38;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.Location = new System.Drawing.Point(375, 454);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(59, 57);
            this.btnCancelar.TabIndex = 37;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardar.BackgroundImage")));
            this.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardar.Location = new System.Drawing.Point(309, 454);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(60, 57);
            this.btnGuardar.TabIndex = 36;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // btnExportarExcel
            // 
            this.btnExportarExcel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExportarExcel.BackgroundImage")));
            this.btnExportarExcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExportarExcel.Location = new System.Drawing.Point(75, 454);
            this.btnExportarExcel.Name = "btnExportarExcel";
            this.btnExportarExcel.Size = new System.Drawing.Size(59, 57);
            this.btnExportarExcel.TabIndex = 40;
            this.btnExportarExcel.UseVisualStyleBackColor = true;
            this.btnExportarExcel.Click += new System.EventHandler(this.BtnExportarExcel_Click);
            // 
            // btnExportarPDF
            // 
            this.btnExportarPDF.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExportarPDF.BackgroundImage")));
            this.btnExportarPDF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExportarPDF.Location = new System.Drawing.Point(13, 454);
            this.btnExportarPDF.Name = "btnExportarPDF";
            this.btnExportarPDF.Size = new System.Drawing.Size(55, 57);
            this.btnExportarPDF.TabIndex = 39;
            this.btnExportarPDF.UseVisualStyleBackColor = true;
            this.btnExportarPDF.Click += new System.EventHandler(this.BtnExportarPDF_Click);
            // 
            // gpoAlumnos
            // 
            this.gpoAlumnos.Controls.Add(this.label5);
            this.gpoAlumnos.Controls.Add(this.label4);
            this.gpoAlumnos.Controls.Add(this.label3);
            this.gpoAlumnos.Controls.Add(this.txtApm);
            this.gpoAlumnos.Controls.Add(this.txtApp);
            this.gpoAlumnos.Controls.Add(this.txtNombreAlumno);
            this.gpoAlumnos.Controls.Add(this.cmbAlumnos);
            this.gpoAlumnos.Location = new System.Drawing.Point(13, 82);
            this.gpoAlumnos.Name = "gpoAlumnos";
            this.gpoAlumnos.Size = new System.Drawing.Size(479, 98);
            this.gpoAlumnos.TabIndex = 41;
            this.gpoAlumnos.TabStop = false;
            this.gpoAlumnos.Text = "Alumnos";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Nombre";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(210, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Apellido paterno";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(336, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Apellido materno";
            // 
            // txtApm
            // 
            this.txtApm.Location = new System.Drawing.Point(339, 72);
            this.txtApm.Name = "txtApm";
            this.txtApm.ReadOnly = true;
            this.txtApm.Size = new System.Drawing.Size(132, 20);
            this.txtApm.TabIndex = 4;
            // 
            // txtApp
            // 
            this.txtApp.Location = new System.Drawing.Point(213, 72);
            this.txtApp.Name = "txtApp";
            this.txtApp.ReadOnly = true;
            this.txtApp.Size = new System.Drawing.Size(123, 20);
            this.txtApp.TabIndex = 3;
            // 
            // txtNombreAlumno
            // 
            this.txtNombreAlumno.Location = new System.Drawing.Point(6, 72);
            this.txtNombreAlumno.Name = "txtNombreAlumno";
            this.txtNombreAlumno.ReadOnly = true;
            this.txtNombreAlumno.Size = new System.Drawing.Size(204, 20);
            this.txtNombreAlumno.TabIndex = 2;
            // 
            // cmbAlumnos
            // 
            this.cmbAlumnos.FormattingEnabled = true;
            this.cmbAlumnos.Location = new System.Drawing.Point(7, 20);
            this.cmbAlumnos.Name = "cmbAlumnos";
            this.cmbAlumnos.Size = new System.Drawing.Size(165, 21);
            this.cmbAlumnos.TabIndex = 0;
            this.cmbAlumnos.SelectedIndexChanged += new System.EventHandler(this.CmbAlumnos_SelectedIndexChanged);
            // 
            // gpoGrupos
            // 
            this.gpoGrupos.Controls.Add(this.cmbGrupo);
            this.gpoGrupos.Location = new System.Drawing.Point(13, 12);
            this.gpoGrupos.Name = "gpoGrupos";
            this.gpoGrupos.Size = new System.Drawing.Size(201, 55);
            this.gpoGrupos.TabIndex = 42;
            this.gpoGrupos.TabStop = false;
            this.gpoGrupos.Text = "Grupos";
            // 
            // cmbGrupo
            // 
            this.cmbGrupo.FormattingEnabled = true;
            this.cmbGrupo.Location = new System.Drawing.Point(6, 20);
            this.cmbGrupo.Name = "cmbGrupo";
            this.cmbGrupo.Size = new System.Drawing.Size(166, 21);
            this.cmbGrupo.TabIndex = 0;
            this.cmbGrupo.SelectedIndexChanged += new System.EventHandler(this.CmbGrupo_SelectedIndexChanged);
            // 
            // gpoMateria
            // 
            this.gpoMateria.Controls.Add(this.cmbMateria);
            this.gpoMateria.Location = new System.Drawing.Point(226, 12);
            this.gpoMateria.Name = "gpoMateria";
            this.gpoMateria.Size = new System.Drawing.Size(266, 55);
            this.gpoMateria.TabIndex = 43;
            this.gpoMateria.TabStop = false;
            this.gpoMateria.Text = "Materia";
            // 
            // cmbMateria
            // 
            this.cmbMateria.FormattingEnabled = true;
            this.cmbMateria.Location = new System.Drawing.Point(6, 20);
            this.cmbMateria.Name = "cmbMateria";
            this.cmbMateria.Size = new System.Drawing.Size(223, 21);
            this.cmbMateria.TabIndex = 2;
            this.cmbMateria.SelectedIndexChanged += new System.EventHandler(this.CmbMateria_SelectedIndexChanged);
            // 
            // dtgCalificaciones
            // 
            this.dtgCalificaciones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dtgCalificaciones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtgCalificaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCalificaciones.Location = new System.Drawing.Point(13, 269);
            this.dtgCalificaciones.Name = "dtgCalificaciones";
            this.dtgCalificaciones.Size = new System.Drawing.Size(479, 165);
            this.dtgCalificaciones.TabIndex = 46;
            this.dtgCalificaciones.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgCalificaciones_CellClick);
            this.dtgCalificaciones.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgCalificaciones_CellDoubleClick);
            // 
            // txtParcial1
            // 
            this.txtParcial1.Location = new System.Drawing.Point(21, 39);
            this.txtParcial1.Name = "txtParcial1";
            this.txtParcial1.Size = new System.Drawing.Size(100, 20);
            this.txtParcial1.TabIndex = 47;
            // 
            // txtParcial4
            // 
            this.txtParcial4.Location = new System.Drawing.Point(339, 39);
            this.txtParcial4.Name = "txtParcial4";
            this.txtParcial4.Size = new System.Drawing.Size(100, 20);
            this.txtParcial4.TabIndex = 48;
            // 
            // txtParcial3
            // 
            this.txtParcial3.Location = new System.Drawing.Point(233, 39);
            this.txtParcial3.Name = "txtParcial3";
            this.txtParcial3.Size = new System.Drawing.Size(100, 20);
            this.txtParcial3.TabIndex = 49;
            // 
            // txtParcial2
            // 
            this.txtParcial2.Location = new System.Drawing.Point(127, 39);
            this.txtParcial2.Name = "txtParcial2";
            this.txtParcial2.Size = new System.Drawing.Size(100, 20);
            this.txtParcial2.TabIndex = 50;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Parcial 1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(336, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 52;
            this.label8.Text = "Parcial 4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(230, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 53;
            this.label9.Text = "Parcial 3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(124, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 54;
            this.label10.Text = "Parcial 2";
            // 
            // gpoCalificaciones
            // 
            this.gpoCalificaciones.Controls.Add(this.txtParcial4);
            this.gpoCalificaciones.Controls.Add(this.label10);
            this.gpoCalificaciones.Controls.Add(this.txtParcial1);
            this.gpoCalificaciones.Controls.Add(this.label9);
            this.gpoCalificaciones.Controls.Add(this.txtParcial3);
            this.gpoCalificaciones.Controls.Add(this.label8);
            this.gpoCalificaciones.Controls.Add(this.txtParcial2);
            this.gpoCalificaciones.Controls.Add(this.label1);
            this.gpoCalificaciones.Location = new System.Drawing.Point(13, 186);
            this.gpoCalificaciones.Name = "gpoCalificaciones";
            this.gpoCalificaciones.Size = new System.Drawing.Size(479, 77);
            this.gpoCalificaciones.TabIndex = 55;
            this.gpoCalificaciones.TabStop = false;
            // 
            // bgwExportar
            // 
            this.bgwExportar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BgwExportar_DoWork);
            this.bgwExportar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BgwExportar_RunWorkerCompleted);
            // 
            // Calificaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(509, 519);
            this.Controls.Add(this.gpoCalificaciones);
            this.Controls.Add(this.dtgCalificaciones);
            this.Controls.Add(this.gpoMateria);
            this.Controls.Add(this.gpoGrupos);
            this.Controls.Add(this.gpoAlumnos);
            this.Controls.Add(this.btnExportarExcel);
            this.Controls.Add(this.btnExportarPDF);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Name = "Calificaciones";
            this.Text = "Calificaciones";
            this.Load += new System.EventHandler(this.Calificaciones_Load);
            this.gpoAlumnos.ResumeLayout(false);
            this.gpoAlumnos.PerformLayout();
            this.gpoGrupos.ResumeLayout(false);
            this.gpoMateria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgCalificaciones)).EndInit();
            this.gpoCalificaciones.ResumeLayout(false);
            this.gpoCalificaciones.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnExportarExcel;
        private System.Windows.Forms.Button btnExportarPDF;
        private System.Windows.Forms.GroupBox gpoAlumnos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtApm;
        private System.Windows.Forms.TextBox txtApp;
        private System.Windows.Forms.TextBox txtNombreAlumno;
        private System.Windows.Forms.ComboBox cmbAlumnos;
        private System.Windows.Forms.GroupBox gpoGrupos;
        private System.Windows.Forms.ComboBox cmbGrupo;
        private System.Windows.Forms.GroupBox gpoMateria;
        private System.Windows.Forms.ComboBox cmbMateria;
        private System.Windows.Forms.DataGridView dtgCalificaciones;
        private System.Windows.Forms.TextBox txtParcial1;
        private System.Windows.Forms.TextBox txtParcial4;
        private System.Windows.Forms.TextBox txtParcial3;
        private System.Windows.Forms.TextBox txtParcial2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gpoCalificaciones;
        private System.ComponentModel.BackgroundWorker bgwExportar;
    }
}