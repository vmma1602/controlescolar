﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;



namespace ControlEscolar2
{
    public partial class Alumnos : Form
    {
       
        private AlumnoManejador _alumnoManejador;
        private EstadoManejador _estadoManejador;
        private MunicipioManejador _municipioManejador;
        public Alumnos()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _estadoManejador = new EstadoManejador();
            _municipioManejador = new MunicipioManejador();
            cmbInvisible.Enabled = false;
           
        }
        
        private void BuscarAlumnos(string filtro)
        {
            dtgAlumnos.DataSource = _alumnoManejador.GetAlumnos(filtro);
        }
        private void ControlarBotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnCancelar.Enabled = cancelar;

        }
        private void ControlarTexto(bool uno)
        {
            txtApm.Enabled = uno;
            txtApp.Enabled = uno;
            txtNombre.Enabled = uno;
            txtDomicilio.Enabled = uno;
            txtEmail.Enabled = uno;
            txtNumControl.Enabled = uno;
            cmbSexo.Enabled = uno;
            dtpAlumno.Enabled = uno;
            cmbEstados.Enabled = uno;
            cmbCiudades.Enabled = uno;
            

        }
        public void Ciudades(string x)
        {           
            cmbInvisible.Visible = false;
            cmbInvisible.DataSource = _estadoManejador.CodigoEstado(x);
            cmbInvisible.DisplayMember = "CodigoEstado";
        }
        private void Alumnos_Load(object sender, EventArgs e)
        {           
            cmbEstados.DataSource = _estadoManejador.GetEstadosList();
            cmbEstados.DisplayMember = "nombre";        
            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            Limpiar();
        }
       
        private void Limpiar()
        {
            txtNumControl.Clear();
            txtNombre.Clear();
            txtEmail.Clear();
            txtDomicilio.Clear();
            txtBuscar.Clear();
            txtApp.Clear();
            txtApm.Clear();
           
            lblIDAlumno.Text = "0";
        }
         
        private void EliminarUsuario()
        {
            var ID = dtgAlumnos.CurrentRow.Cells["ID"].Value;
          _alumnoManejador.Eliminar(Convert.ToInt32(ID));
        }
        private void GuardarAlumnos()
        {

            int g = dtpAlumno.Value.Year;
            int w = dtpAlumno.Value.Month;
            int y = dtpAlumno.Value.Day;
            string z = g.ToString() + "/" + w.ToString() + "/" + y.ToString();
            _alumnoManejador.Guardar(new Entidades.ControlEscolar2.Alumnos
            {
              
                ID = int.Parse(lblIDAlumno.Text),
                NumControl = int.Parse(txtNumControl.Text),
                Nombre = txtNombre.Text,
                App = txtApp.Text,
                Apm = txtApm.Text,
                Domicilio = txtDomicilio.Text,
                Email = txtEmail.Text,
                Fecha=z,
                Sexo = cmbSexo.Text,
                Estado = cmbEstados.Text,
                Ciudad = cmbCiudades.Text

            }); 

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarTexto(true);
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            try
            {
                GuardarAlumnos();
                BuscarAlumnos("");
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            Limpiar();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro de borrar registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarUsuario();
                    BuscarAlumnos("");
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }
        private void ModificarAlumnos()
        {

            ControlarBotones(false, true, true, false);
            ControlarTexto(true);
            var NumControl = dtgAlumnos.CurrentRow.Cells["NumControl"].Value;
            var ID = dtgAlumnos.CurrentRow.Cells["ID"].Value;
            lblIDAlumno.Text = ID.ToString();
            txtNumControl.Text = NumControl.ToString();
            txtApp.Text = dtgAlumnos.CurrentRow.Cells["app"].Value.ToString();
            txtApm.Text = dtgAlumnos.CurrentRow.Cells["apm"].Value.ToString();
            txtNombre.Text = dtgAlumnos.CurrentRow.Cells["nombre"].Value.ToString();
            txtDomicilio.Text = dtgAlumnos.CurrentRow.Cells["domicilio"].Value.ToString();
            txtEmail.Text = dtgAlumnos.CurrentRow.Cells["email"].Value.ToString();          
            cmbSexo.Text = dtgAlumnos.CurrentRow.Cells["sexo"].Value.ToString();
            cmbCiudades.Text= dtgAlumnos.CurrentRow.Cells["ciudad"].Value.ToString();
            cmbEstados.Text= dtgAlumnos.CurrentRow.Cells["estado"].Value.ToString();
        }

        private void DtgAlumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAlumnos();
                BuscarAlumnos("");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAlumnos(txtBuscar.Text);
        }

        private void CmbEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            Ciudades(cmbEstados.Text);
            cmbCiudades.DataSource = _municipioManejador.GetCiudadesList(cmbInvisible.Text);
            cmbCiudades.DisplayMember = "nombreMun";
            
        }
      
      
    }
}
