﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class frmEditarEstudios : Form
    {
        private EstudioManejador _estudioManejador;
        private EstadoManejador _estadoManejador;
        private MunicipioManejador _municipioManejador;
        private MaestroManejador _maestroManejador;
       

        
        public frmEditarEstudios()
        {
            InitializeComponent();
            _estadoManejador = new EstadoManejador();
            _municipioManejador = new MunicipioManejador();
            _estudioManejador = new EstudioManejador();
            _maestroManejador = new MaestroManejador();
        }

        private void FrmEditarEstudios_Load(object sender, EventArgs e)
        {
            dtgEstudios.DataSource = _estudioManejador.GetEstudios("");
        }
    }
}
