﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
namespace ControlEscolar2
{
    public partial class Grupos : Form
    {
        AlumnoManejador _alumnoManejador;
        GrupoManejador _grupoManejador;
        Asignacion_AlumnosManejador _asignacion_AlumnosManejador;
        Maestros_GruposManejador _maestros_GruposManejador;
        MateriaManejador _materiaManejador;
        MaestroManejador _maestroManejador;
        Grupos_MateriasManejador _grupos_MateriasManejador;


        int IDAlumno_Num = 0;
        int Editar_asignacion;
        int numControl_Global = 0;
        int fkGrupo_Global = 0;
        int IDMaestro_GrupoINT = 0;


        public Grupos()
        {
            InitializeComponent();
            _grupoManejador = new GrupoManejador();
            _asignacion_AlumnosManejador = new Asignacion_AlumnosManejador();
            _alumnoManejador = new AlumnoManejador();
            _maestros_GruposManejador = new Maestros_GruposManejador();
            _materiaManejador = new MateriaManejador();
            _maestroManejador = new MaestroManejador();
            _grupos_MateriasManejador = new Grupos_MateriasManejador();
        }
        private void MostrarGrupos()
        {
            dtgGrupo.DataSource = _grupoManejador.GetGrupo("");
        }
        private void MostrarAsignacion(string filtro_nombreGrupo)
        {
            dtgAsigancion_Alumnos.DataSource = _asignacion_AlumnosManejador.GetAsignacion_Alumnos(filtro_nombreGrupo);
        }
        private void MostrarAlumnos()
        {
            dtgAlumnos.DataSource = _alumnoManejador.GetAlumnos("");
        }
        private void MostrarMaestros_Grupos()
        {
            dtgMaestros_Grupos.DataSource = _maestros_GruposManejador.GetMaestros_Grupos("");
        }
        private void CargarCombo_Asignacion()
        {
            cmbClaveGrupo.DataSource = _grupoManejador.GetGrupo("");
            cmbClaveGrupo.DisplayMember = "nombreGrupo";
            comboBox1.DataSource = _grupoManejador.GetGrupo("");
            comboBox1.DisplayMember = "nombreGrupo";
            cmbClaveMateria.DataSource = _materiaManejador.GetMaterias("");
            cmbClaveMateria.DisplayMember = "IDMateria";

        }
         void CargarCombos_Maestros_Grupos()
        {
            cmbClaveMaestro.DataSource = _maestroManejador.NumControlMaestro().Tables[0];
            cmbClaveMaestro.DisplayMember = "NumControl";
            cmbClaveGrupos.DataSource = _grupoManejador.GetGrupo("");
            cmbClaveGrupos.DisplayMember = "nombreGrupo";          
            cmbClaveMateria.DataSource = _materiaManejador.GetMaterias("");
            cmbClaveMateria.DisplayMember = "IDMateria";           
        }
        private int ComprobarCampos()
        {
            if (txtNombreGrupo.Text == "" || cmbCarrera.Text == "" || cmbSemestre.Text == "" || cmbTurno.Text == "")
            {               
                return 1;
            }
            else
                return 0;
        }
        private int ComprobarClaveGrupo()
        {
            var ds = _grupoManejador.IDGrupoDS();
            if (ds.Tables[0].Rows.Count==0)
            {  }
            else
            {
               string nombreGrupo = ds.Tables[0].Rows[0]["nombreGrupo"].ToString();
                if(nombreGrupo==txtNombreGrupo.Text)
                {
                    MessageBox.Show("Ese grupo ya está creado");
                    return 1;
                }
                return 0;
            }
            return 0;
        }
        void Limpiar()
        {
            txtNombreGrupo.Clear();
            cmbCarrera.Text = "";
            cmbSemestre.Text = "";
            cmbTurno.Text = "";
            lblIDGrupo.Text = "0";
        }
        private string ArmarClaveGrupo()
        {
            string Carrera_Abreviada="";
            string semestre_string;
            string turno_string;
            string clave_Armada="";

          /*
            else
            {*/
                try
                {
                    switch (cmbCarrera.SelectedIndex)
                    {
                        case 0:
                            Carrera_Abreviada = "ISC";
                            break;
                        case 1:
                            Carrera_Abreviada = "IGE";
                            break;
                        case 2:
                            Carrera_Abreviada = "II";
                            break;
                        case 3:
                            Carrera_Abreviada = "IE";
                            break;
                        case 4:
                            Carrera_Abreviada = "IC";
                            break;
                        case 5:
                            Carrera_Abreviada = "IA";
                            break;
                        default:

                            break;
                    }
                    semestre_string = cmbSemestre.Text;
                    turno_string = cmbTurno.Text.Substring(0, 1);
                    clave_Armada = Carrera_Abreviada + semestre_string + turno_string;
                }
                catch { }
           
            return clave_Armada;
        }
        void GuardarGrupo()
        {
            int x;
            if (ComprobarCampos()==0)
            {

                if (ComprobarClaveGrupo()==0)
                {
                    _grupoManejador.Guardar(new Entidades.ControlEscolar2.Grupo
                    {
                        NombreGrupo = txtNombreGrupo.Text,
                        Turno = cmbTurno.Text,
                        Semestre = int.Parse(cmbSemestre.Text),
                        Carrera = cmbCarrera.Text



                    }, int.Parse(lblIDGrupo.Text));
                    MostrarGrupos();
                }
               
            }
            else
            {
                MessageBox.Show("Debe llenar todos los campos");
            }
        }
        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void Label13_Click(object sender, EventArgs e)
        {

        }

        private void Grupos_Load(object sender, EventArgs e)
        {
            MostrarGrupos();
            MostrarAlumnos();            
            CargarCombo_Asignacion();
            CargarCombos_Maestros_Grupos();
            txtClaveAsignacion.Text = "";
            MostrarMaestros_Grupos();
        }

        private void CmbTurno_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNombreGrupo.Text= ArmarClaveGrupo();
        }

        private void BtnGuardarGrupo_Click(object sender, EventArgs e)
        {
            GuardarGrupo();
            MostrarGrupos();
            CargarCombo_Asignacion();
            CargarCombos_Maestros_Grupos();
        }

        private void CmbSemestre_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNombreGrupo.Text = ArmarClaveGrupo();
        }

        private void CmbCarrera_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNombreGrupo.Text = ArmarClaveGrupo();
        }

        private void BtnCancelarGrupo_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void DtgGrupo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            lblIDGrupo.Text = dtgGrupo.CurrentRow.Cells["IDGrupo"].Value.ToString();
            int IDGrupo = int.Parse(lblIDGrupo.Text);
            
        }

        private void BtnEliminarGrupo_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro de borrar registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _grupoManejador.Eliminar(int.Parse(lblIDGrupo.Text));
                MostrarGrupos();
                CargarCombo_Asignacion();
                CargarCombos_Maestros_Grupos();

            }
        }

        private void DtgGrupo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            lblIDGrupo.Text = dtgGrupo.CurrentRow.Cells["IDGrupo"].Value.ToString();
            cmbCarrera.Text= dtgGrupo.CurrentRow.Cells["Carrera"].Value.ToString();
            cmbTurno.Text = dtgGrupo.CurrentRow.Cells["Turno"].Value.ToString();
            cmbSemestre.Text = dtgGrupo.CurrentRow.Cells["Semestre"].Value.ToString();
           
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ds = _asignacion_AlumnosManejador.nombreGrupoDS(cmbClaveGrupo.Text);

            if (ds.Tables[0].Rows.Count == 0)
            { }
            else
            {
                lblIDasignacion.Text = ds.Tables[0].Rows[0]["IDGrupo"].ToString();
                fkGrupo_Global = int.Parse(lblIDasignacion.Text);
            }

        }

        private void DtgAlumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            lblIDAlumno.Text = dtgAlumnos.CurrentRow.Cells["ID"].Value.ToString();
            IDAlumno_Num = int.Parse(lblIDAlumno.Text);            
            txtNombreAlumno.Text= dtgAlumnos.CurrentRow.Cells["Nombre"].Value.ToString();
            txtNumControl.Text = dtgAlumnos.CurrentRow.Cells["NumControl"].Value.ToString();
            txtAppAlumno.Text = dtgAlumnos.CurrentRow.Cells["App"].Value.ToString();
            txtApmAlumno.Text = dtgAlumnos.CurrentRow.Cells["Apm"].Value.ToString();
            txtDomicilioAlumno.Text = dtgAlumnos.CurrentRow.Cells["Domicilio"].Value.ToString();
            txtEstadoAlumno.Text = dtgAlumnos.CurrentRow.Cells["Estado"].Value.ToString();
            txtEmailAlumno.Text = dtgAlumnos.CurrentRow.Cells["Email"].Value.ToString();
            txtMunicipioAlumno.Text = dtgAlumnos.CurrentRow.Cells["Ciudad"].Value.ToString();
           

        }
        void LimpiarAsignacion()
        {
            lblIDAlumno.Text = "0";
            lblIDasignacion.Text = "0";
            Editar_asignacion = 0;
            IDAlumno_Num = 0;
            txtNombreAlumno.Clear();
            txtNumControl.Clear();
            txtAppAlumno.Clear();
            txtApmAlumno.Clear();
            txtDomicilioAlumno.Clear();
            txtEstadoAlumno.Clear();
            txtEmailAlumno.Clear();
            txtMunicipioAlumno.Clear();
            numControl_Global = 0;
            fkGrupo_Global = 0;
        }

        private void CmbClaveGrupo_TextChanged(object sender, EventArgs e)
        {
                
        }

        private void BtnGuardarGrupo_Alumnos_Click(object sender, EventArgs e)
        {
            if (ComprobrarAlumnoenGrupo() == 0)
            {

                _asignacion_AlumnosManejador.Guardar(new Entidades.ControlEscolar2.Asignacion_Alumnos
                {
                    NombreGrupo = cmbClaveGrupo.Text,
                    NumControl = int.Parse(txtNumControl.Text),
                    FkAlumno = int.Parse(lblIDAlumno.Text),
                    FkGrupo = int.Parse(lblIDasignacion.Text)

                }, Editar_asignacion);
                MostrarAsignacion(comboBox1.Text);
                LimpiarAsignacion();
                CargarCombos_Maestros_Grupos();
            }
            else if (ComprobrarAlumnoenGrupo() == 20)
            {
               _asignacion_AlumnosManejador.Guardar(new Entidades.ControlEscolar2.Asignacion_Alumnos
                {
                    NombreGrupo = cmbClaveGrupo.Text,
                    NumControl = numControl_Global,
                    FkAlumno = int.Parse(lblIDAlumno.Text),
                    FkGrupo = fkGrupo_Global

                }, Editar_asignacion);

                MostrarAsignacion(comboBox1.Text);
                LimpiarAsignacion();
                CargarCombos_Maestros_Grupos();
            }
            else
                MessageBox.Show("Ese alumno ya está asignado a un grupo");
                  LimpiarAsignacion();
        }

        private void DtgAsigancion_Alumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string valorIDAsignacion = dtgAsigancion_Alumnos.CurrentRow.Cells["AsignacionID"].Value.ToString();
            Editar_asignacion = int.Parse(valorIDAsignacion);
            string IDAlumno = dtgAsigancion_Alumnos.CurrentRow.Cells["fkAlumno"].Value.ToString();
            int alumno = int.Parse(IDAlumno);
            lblIDAlumno.Text = IDAlumno;
            lblIDasignacion.Text = Editar_asignacion.ToString();
           
            var ds = _asignacion_AlumnosManejador.TodoAlumnos(alumno);
            if (ds.Tables[0].Rows.Count == 0)
            { }
            else
            {
                 string v = ds.Tables[0].Rows[0]["NumControl"].ToString();
                 numControl_Global = int.Parse(v);
                 string v1=  ds.Tables[0].Rows[0]["IDGrupo"].ToString();
                fkGrupo_Global = int.Parse(v1);
              
                txtNombreAlumno.Text = ds.Tables[0].Rows[0]["nombre"].ToString();
                
                txtAppAlumno.Text = ds.Tables[0].Rows[0]["app"].ToString();
                txtApmAlumno.Text = ds.Tables[0].Rows[0]["apm"].ToString();
                txtDomicilioAlumno.Text = ds.Tables[0].Rows[0]["domicilio"].ToString();
                txtEstadoAlumno.Text = ds.Tables[0].Rows[0]["estado"].ToString();
                txtEmailAlumno.Text = ds.Tables[0].Rows[0]["email"].ToString();
                txtMunicipioAlumno.Text = ds.Tables[0].Rows[0]["ciudad"].ToString();
               // cmbClaveGrupo.Text= ds.Tables[0].Rows[0]["nombreGrupo"].ToString();
            }
        }

        private void DtgAsigancion_Alumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
         
        }

        private void BtnCancelarGrupo_Alumnos_Click(object sender, EventArgs e)
        {
            LimpiarAsignacion();
        }
        private int ComprobrarAlumnoenGrupo()
        {
            if (txtNumControl.Text == "")
            {
               
                return 20;
            }
            else
            {
                DataSet ds = _asignacion_AlumnosManejador.ComprobarAlummnoenGrupo(int.Parse(txtNumControl.Text));
                if (ds.Tables[0].Rows.Count == 0)
                {                   
                    return 0;
                }
                else
                    return 1;
            }
            
        }

        private void BtnEliminarGrupo_Alumnos_Click(object sender, EventArgs e)
        {
           
            if (MessageBox.Show("¿Estas seguro de borrar registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {               
                _asignacion_AlumnosManejador.Eliminar(Editar_asignacion);
                MostrarAsignacion(comboBox1.Text);
                LimpiarAsignacion();
                CargarCombos_Maestros_Grupos();
            }
        }

        private void DtgAsigancion_Alumnos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string valorIDAsignacion = dtgAsigancion_Alumnos.CurrentRow.Cells["AsignacionID"].Value.ToString();
            Editar_asignacion = int.Parse(valorIDAsignacion);
        }

        private void ComboBox1_TextChanged(object sender, EventArgs e)
        {          
            MostrarAsignacion(comboBox1.Text);
        }

        private void CmbClaveGrupos_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void BtnGuardarMaestro_Grupo_Click(object sender, EventArgs e)
        {
            if(txtClaveAsignacion.Text=="")
            {

            }
            else
            {
                
                _maestros_GruposManejador.Guardar(new Entidades.ControlEscolar2.Maestros_Grupos
                {

                    ClaveMateria = cmbClaveMateria.Text,
                    ClaveGrupo = cmbClaveGrupos.Text,
                    ClaveMaestro = cmbClaveMaestro.Text
                }, IDMaestro_GrupoINT);               
                MostrarMaestros_Grupos();
                txtClaveAsignacion.Text = "0";
            }
         
        }
        private string ArmarClaveMAestros_Grupos()
        {
            try
            {
                return cmbClaveMaestro.Text + "-" + cmbClaveGrupos.Text + "-" + cmbClaveMateria.Text;
            }
            catch { return ""; }
        }

        private void CmbClaveMaestro_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void CmbClaveMateria_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtClaveAsignacion.Text = ArmarClaveMAestros_Grupos();
        }

        private void DtgMaestros_Grupos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string IDMaestro_Grupo = dtgMaestros_Grupos.CurrentRow.Cells["IDMaestros_Grupos"].Value.ToString();
            IDMaestro_GrupoINT = int.Parse(IDMaestro_Grupo);
            label19.Text = IDMaestro_Grupo;
        }

        private void BtnEliminarMaestro_Grupo_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro de borrar registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _maestros_GruposManejador.Eliminar(IDMaestro_GrupoINT);
                MostrarMaestros_Grupos();
                label19.Text = "0";
                IDMaestro_GrupoINT = 0;
            }
        }

        private void TabPage2_Click(object sender, EventArgs e)
        {

        }
    }
}
