﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios.ControlEscolar2;
using Entidades.ControlEscolar2;
namespace ControlEscolar2
{
    public partial class Materias : Form
    {
        DataSet ds, dsLiga, IDGeneral;

        MateriaManejador _materiaManejador;
        LigaManejador _ligaManejador;
        string IDLiga ;
        int IDLigaNum, claveNumIDMateria=0;
        public Materias()
        {
            InitializeComponent();
            _materiaManejador = new MateriaManejador();
            _ligaManejador = new LigaManejador();
        }

        private void Materias_Load(object sender, EventArgs e)
        {
            MessageBox.Show("La clave de la materia debe contener dos letras distintivas y un numero dependiendo de su nivel");
            GetMaterias("");
          dtgMaterias.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
          dtgLiga.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            lblClaveMateria.Text = "0";
        }
        void GuardarMaterias()
        {
          
                int x = 0;
                string a = txtIDMateria.Text;
                 string v = a;
                 a = a.Substring(a.Length - 3, 2); //Letras clave de el ID de la materia
            
            if (int.TryParse(a, out x))
            {
                MessageBox.Show("La clave de la materia no tiene el formato correcto");
            }
            else
            {
                try {
                    v = v.Substring(v.Length - 1); //Numero del ID             
                    int numID = int.Parse(v);


                    _materiaManejador.Guardar(new Entidades.ControlEscolar2.Materias
                    {
                        IDMateria = txtIDMateria.Text,
                        NombreMateria = txtNombre.Text,
                        HorasPractica = int.Parse(txtPractica.Text),
                        HorasTeoria = int.Parse(txtTeoria.Text)
                    }, lblClaveMateria.Text, claveNumIDMateria);
                    
                }
                catch
                {
                    MessageBox.Show("Datos erroneos, no se ingresó el registro");
                }
                }

            
          

        }
        void GetMaterias(string filtro)
        {
            dtgMaterias.DataSource = _materiaManejador.GetMaterias(filtro);
            dtgLiga.DataSource = _ligaManejador.GetLiga(filtro);
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            GuardarMaterias();
            claveNumIDMateria = 0;
            BuscarLiga(txtIDMateria.Text); // Esto va despues de ingresar una materia            
            GetMaterias("");
            txtIDMateria.ReadOnly = false;
            IDLigaNum = 0;
            Limpiar();
          
        }
        void BuscarLiga(string filtro)
        {
            try
            {
                ds = _materiaManejador.MaxIDMateria(filtro);

                if (ds.Tables[0].Rows.Count == 0)
                { lblClaveMateria.Text = "0"; claveNumIDMateria = 0; GuardarMaterias(); BuscarLiga(txtIDMateria.Text); }
                else
                {
                    string a = ds.Tables[0].Rows[0]["IDMateria"].ToString();
                    string v = a;
                    a = a.Substring(a.Length - 3, 2); //Letras clave de el ID de la materia                 
                    v = v.Substring(v.Length - 1); //Numero del ID             
                    int numID = int.Parse(v);

                    dsLiga = _ligaManejador.dsLiga(a);
                    //Aqui empieza el desmadre con los DataSet
                    if (dsLiga.Tables[0].Rows.Count == 0)
                    {
                        if (numID == 1)
                        {
                            _ligaManejador.Guardar(new Entidades.ControlEscolar2.Liga
                            {
                                IDMateria = "",
                                Primera = txtIDMateria.Text,
                                Tercera = ""

                            });
                        }
                        else if (numID == 2)
                        {
                            _ligaManejador.Guardar(new Entidades.ControlEscolar2.Liga
                            {
                                IDMateria = txtIDMateria.Text,
                                Primera = "",
                                Tercera = ""

                            });
                        }
                        else if (numID == 3)
                        {
                            _ligaManejador.Guardar(new Entidades.ControlEscolar2.Liga
                            {
                                IDMateria = "",
                                Primera = "",
                                Tercera = txtIDMateria.Text

                            });
                        }
                    }
                    else
                    {
                        string IDLiga = dsLiga.Tables[0].Rows[0]["IDLiga"].ToString();                      
                        IDLigaNum = int.Parse(IDLiga);
                        if (numID == 1)
                        {
                            _ligaManejador.ActualizarPrimero(new Entidades.ControlEscolar2.Liga
                            {
                                Primera = txtIDMateria.Text
                            }, IDLigaNum);
                        }
                        else if (numID == 2)
                        {

                            _ligaManejador.ActualizarSegundo(new Entidades.ControlEscolar2.Liga
                            {
                                IDMateria = txtIDMateria.Text
                            }, IDLigaNum);
                        }
                        else if (numID == 3)
                        {
                            _ligaManejador.ActualizarTercero(new Entidades.ControlEscolar2.Liga
                            {
                                Tercera = txtIDMateria.Text
                            }, IDLigaNum);
                        }
                    }
                }
            }
            catch
            {
                
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            BorrarLigayMateria();
            ComprobarBorrado();
            GetMaterias("");
            lblClaveMateria.Text = "0";
            Limpiar();
        }

        private void DtgMaterias_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            lblClaveMateria.Text = dtgMaterias.CurrentRow.Cells["IDMateria"].Value.ToString();
            IDGeneral = _ligaManejador.dsLiga(lblClaveMateria.Text);
            if (IDGeneral.Tables[0].Rows.Count==0)
            { }           
            else
            IDLiga = IDGeneral.Tables[0].Rows[0]["IDLiga"].ToString();
            
            
        }
        void BorrarLigayMateria()
        {
            try
            {
                lblClaveMateria.Text = dtgMaterias.CurrentRow.Cells["IDMateria"].Value.ToString();

                ds = _materiaManejador.MaxIDMateria(lblClaveMateria.Text);
                dsLiga = _ligaManejador.dsLiga(lblClaveMateria.Text);
                string a = ds.Tables[0].Rows[0]["IDMateria"].ToString();
                _materiaManejador.Eliminar(a);
                string v = a;
                a = a.Substring(a.Length - 3, 2); //Letras clave de el ID de la materia                 
                v = v.Substring(v.Length - 1); //Numero del ID             
                int numID = int.Parse(v);

                try
                {
                    dsLiga.Tables[0].Rows[0]["IDLiga"].ToString();
                    IDLigaNum = int.Parse(IDLiga);
                    if (numID == 1)
                    {
                        _ligaManejador.ActualizarPrimero(new Entidades.ControlEscolar2.Liga
                        {
                            Primera = ""
                        }, IDLigaNum);
                    }
                    else if (numID == 2)
                    {

                        _ligaManejador.ActualizarSegundo(new Entidades.ControlEscolar2.Liga
                        {
                            IDMateria = ""
                        }, IDLigaNum);
                    }
                    else if (numID == 3)
                    {
                        _ligaManejador.ActualizarTercero(new Entidades.ControlEscolar2.Liga
                        {
                            Tercera = ""
                        }, IDLigaNum);
                    }
                }
                catch { }

                GetMaterias("");
            }
            catch
            {
                MessageBox.Show("Debes borrar desde la otra tabla la materia en especifico");
            }
        }

        private void DtgMaterias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtIDMateria.ReadOnly = true;
            txtIDMateria.Text = dtgMaterias.CurrentRow.Cells["IDMateria"].Value.ToString();
            txtNombre.Text= dtgMaterias.CurrentRow.Cells["NombreMateria"].Value.ToString();
            txtTeoria.Text=dtgMaterias.CurrentRow.Cells["HorasTeoria"].Value.ToString();
            txtPractica.Text= dtgMaterias.CurrentRow.Cells["HorasPractica"].Value.ToString();
            lblClaveMateria.Text = "1";
            ds = _materiaManejador.IDAutoincrementable(txtIDMateria.Text);
            string valor = ds.Tables[0].Rows[0]["IDMateriaAuto"].ToString();
            claveNumIDMateria = int.Parse(valor);
            
        }

        void ComprobarBorrado()
        {
            dsLiga = _ligaManejador.ObtenerTodo();
            try
            {
                string IDLiga = dsLiga.Tables[0].Rows[0]["IDLiga"].ToString();
                int IDLigaNum = int.Parse(IDLiga);
                string a1 = dsLiga.Tables[0].Rows[0]["IDMateriaActual"].ToString();
                string a2 = dsLiga.Tables[0].Rows[0]["fkAnterior"].ToString();
                string a3 = dsLiga.Tables[0].Rows[0]["fkDespues"].ToString();

                if (a1 == "" && a2 == "" && a3 == "")
                {
                    _ligaManejador.Eliminar(IDLigaNum);
                }
            }
            catch { }
        }
        void Limpiar()
        {
            txtIDMateria.Clear();
            txtNombre.Clear();
            txtPractica.Clear();
            txtTeoria.Clear();
        }
    }
}


