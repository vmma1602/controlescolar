﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.IO;
using System.Threading;

namespace ControlEscolar2
{
    public partial class Escuela : Form
    {
        
        EscuelaManejador _escuelaManejador;
        OpenFileDialog _Doc;
        private string _rutaDoc;
        string ruta;
        DataSet ds;
        int v = 0;
        public Escuela()
        {
            InitializeComponent();
           _escuelaManejador = new EscuelaManejador();
            _Doc = new OpenFileDialog();          
            _rutaDoc = Application.StartupPath + "\\Logo\\";
            

        }
        
        void LlenarCampos()
        {
            ds = _escuelaManejador.IDUnico("");
            if (ds.Tables[0].Rows.Count==0)
            {  }
            else
            {
                lblIDEscuela.Text = ds.Tables[0].Rows[0]["IDEscuela"].ToString();
                lblLogo.Text = ds.Tables[0].Rows[0]["logoNombre"].ToString();
                txtNombre.Text = ds.Tables[0].Rows[0]["NombreEscuela"].ToString();
                txtRfc.Text = ds.Tables[0].Rows[0]["rfc"].ToString();
                txtDomicilio.Text = ds.Tables[0].Rows[0]["domicilio"].ToString();
                txtTelefono.Text = ds.Tables[0].Rows[0]["telefono"].ToString();
                txtEmail.Text = ds.Tables[0].Rows[0]["email"].ToString();
                txtPaginaWeb.Text = ds.Tables[0].Rows[0]["PagWeb"].ToString();
                txtDirector.Text = ds.Tables[0].Rows[0]["nombreDir"].ToString();
                this.pctbxLogo.ImageLocation = _rutaDoc + lblLogo.Text;
                //pctbxLogo.Image.Dispose();
            }
        }
        public void BorrarA(String archivo)
        {
            if (File.Exists(@archivo))
            {
                try
                {
                    File.Delete(@archivo);
                    
                }
                catch (IOException e)
                {
                   
                }
            }
        }
      
        private void CargarDoc()
        {
            _Doc.Filter = "Imagen JPG|*.jpg";
            _Doc.Title = "Cargar archivode imagen";
            _Doc.ShowDialog();
            if (_Doc.FileName != "")
            {
                var archivo = new FileInfo(_Doc.FileName);
                double peso = archivo.Length;
                if (peso > 5000000)
                {
                    MessageBox.Show("No es posible realizar acción, la imagen excede los 5 MB");
                    _rutaDoc = "";
                    _Doc.FileName = null;
                }
                else
                    lblLogo.Text = archivo.Name;
                    pctbxLogo.Image = Image.FromFile(archivo.FullName);

            }
        }
        private void GuardarDoc()
        {

            if (_Doc.FileName != null)
            {
                if (_Doc.FileName != "")
                {
                    var archivo = new FileInfo(_Doc.FileName);                  
                    if (Directory.Exists(_rutaDoc))
                    {
                      
                        var obtenerArchivos = Directory.GetFiles(_rutaDoc,"*.jpg");
                        FileInfo archivoAnterior;
                      
                        if (obtenerArchivos.Length != 0)
                        {
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                          
                            if (archivoAnterior.Exists)
                            {
                                if (archivoAnterior.Name == archivo.Name)
                                {
                                    MessageBox.Show("El archivo ya existe");
                                }
                                else
                                {                                  
                                   archivoAnterior.Delete();                                  
                                   archivo.CopyTo(_rutaDoc + archivo.Name);
                                    MessageBox.Show("Archivo creado");
                                }
                               

                            }
                        }
                        else
                        {

                           
                            archivo.CopyTo(_rutaDoc + archivo.Name);
                            MessageBox.Show("Archivo creado");
                        }

                      
                    }
                    else
                    {

                        Directory.CreateDirectory(_rutaDoc);
                        archivo.CopyTo(_rutaDoc + archivo.Name);

                    }
                    

                }
            }
        }
        private void EliminarDoc()
        {
            if (MessageBox.Show("Esta seguro de borrar el archivo de imagen ", "Eliminar Imagen", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                pctbxLogo.Image = null;
                if (Directory.Exists(_rutaDoc))
                {
                   
                    //
                    var obtenerArchivos = Directory.GetFiles(_rutaDoc, "*.jpg");
                    FileInfo archivoAnterior;
                    if (obtenerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                           
                                pctbxLogo.Image = null;                            
                                BorrarA(archivoAnterior.FullName);
                                //archivoAnterior.Delete();
                                lblLogo.Text = "";
                           
                            

                        }

                    }

                }
            }

        }
        private void EliminarImagen()
        {           
                pctbxLogo.Image = null;
            if (Directory.Exists(_rutaDoc))
            {
                
                var obtenerArchivos = Directory.GetFiles(_rutaDoc, "*.jpg");
                    FileInfo archivoAnterior;
                    if (obtenerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {

                        
                       
                        archivoAnterior.Delete();
                        lblLogo.Text = "";


                        }

                    }

            }          
        }
        int BuscarEscuela()
        {
            ds = _escuelaManejador.IDUnico("");
            if (ds.Tables[0].Rows.Count == 0)
            {
                return 0;
            }
            else
                return 1;
        }

      
        
        private void Escuela_Load(object sender, EventArgs e)
        {
                                     
                pctbxLogo.SizeMode = PictureBoxSizeMode.StretchImage;

            if (BuscarEscuela() == 0)
            {
                lblIDEscuela.Text = "0";
                ControlarTexto(true);
                pctbxLogo.Image = null;
                Limpiar();
                v = 0;
            }
            else
            {
                LlenarCampos();
                ControlarTexto(false);
               
            }
           

        }
        void ControlarTexto(bool control)
        {
            txtDirector.Enabled = control;
            txtDomicilio.Enabled = control;
            txtEmail.Enabled = control;
            txtNombre.Enabled = control;
            txtPaginaWeb.Enabled = control;
            txtRfc.Enabled = control;
            txtTelefono.Enabled = control;
            btnCargar.Enabled = control;
            btnBorrarImagen.Enabled = control;
            
        }
        void ControlarBotones(bool guardar, bool cancelar, bool editar, bool eliminar)
        {
            btnEditar.Enabled = editar;
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnCancelar.Enabled = cancelar; ;
        }


        private void BtnCargar_Click(object sender, EventArgs e)
        {
            CargarDoc();
        }
        public int validar()
        {
            if (txtDirector.Text == "" || txtDomicilio.Text == "" || txtEmail.Text == "" ||
               txtNombre.Text == "" || txtPaginaWeb.Text == "" || txtRfc.Text == "" ||
               txtTelefono.Text == "")
            {
                return 1;
            }
            else
                return 0;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (lblLogo.Text == "")
            {
                MessageBox.Show("Debe insertar una imagen para el logo");              
            }
            else
            {
                if (validar() == 0)
                {
                    _escuelaManejador.Guardar(new Entidades.ControlEscolar2.Escuela
                    {
                        NombreEscuela = txtNombre.Text,
                        NumTel = txtTelefono.Text,
                        LogoNombre = lblLogo.Text,
                        Director = txtDirector.Text,
                        Domicilio = txtDomicilio.Text,
                        Email = txtEmail.Text,
                        PagWeb = txtPaginaWeb.Text,
                        Rfc = txtRfc.Text

                    }, int.Parse(lblIDEscuela.Text));
                    
                    GuardarDoc();
                    ControlarTexto(false);
                    LlenarCampos();
                    
                   
                }
                else
                {
                    MessageBox.Show("Debe llenar todos los campos");
                }
            }
        }
       

        private void BtnBorrarImagen_Click(object sender, EventArgs e)
        {
            EliminarDoc();          
            lblLogo.Text= "";
        }

        private void DtgEscuela_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
             
          
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de borrar el registro?", "Eliminar datos de la escuela", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                _escuelaManejador.Eliminar();
                pctbxLogo.Image = null;
                BorrarA(_rutaDoc + lblLogo.Text);
               // EliminarImagen();
                ControlarTexto(true);
                Limpiar();

            }
        }
        void Limpiar()
        {
            txtDirector.Clear();
            txtDomicilio.Clear();
            txtEmail.Clear();
            txtNombre.Clear();
            txtPaginaWeb.Clear();
            txtRfc.Clear();
            txtTelefono.Clear();
            lblIDEscuela.Text = "0";
            lblLogo.Text = "";
           
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
               
                if (BuscarEscuela() == 0)
                { EliminarImagen(); Limpiar(); }

                else
                {
                    string ruta = ds.Tables[0].Rows[0]["logoNombre"].ToString();
                    pctbxLogo.Image = Image.FromFile(_rutaDoc + ruta);
                    ControlarTexto(false);
                }
            }
            catch { }
            LlenarCampos();        
            
          
        }

        private void DtgEscuela_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            if (BuscarEscuela() == 0)
            {
                lblIDEscuela.Text = "0";
                ControlarTexto(true);
                pctbxLogo.Image = null;
                Limpiar();
            }
            else
            {
                LlenarCampos();                
                ControlarTexto(true);

            }
          
        }

        private void Escuela_MouseHover(object sender, EventArgs e)
        {
            
        }

        private void BtnEliminar_MouseHover(object sender, EventArgs e)
        {
            
        }

        private void PctbxLogo_MouseHover(object sender, EventArgs e)
        {
         
        }
    }
}
