﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.IO;
namespace ControlEscolar2
{
    public partial class Maestros : Form
    {
        OpenFileDialog _Doc;
        private EstudioManejador _estudioManejador;
        private EstadoManejador _estadoManejador;
        private MunicipioManejador _municipioManejador;
        private MaestroManejador _maestroManejador;
        private string _rutaDoc;
        string a;
        public Maestros()
        {
            InitializeComponent();
            _Doc = new OpenFileDialog();
            _estadoManejador = new EstadoManejador();
            _municipioManejador = new MunicipioManejador();
            _estudioManejador = new EstudioManejador();
            _maestroManejador = new MaestroManejador();
            cmbInvisible.Enabled = false;
            _rutaDoc = Application.StartupPath + "\\Documentos\\";
        }
        private void CargarDoc()
        {
            _Doc.Filter = "Archivo PDF|*.pdf";
            _Doc.Title = "Cargar archivo PDF";
            _Doc.ShowDialog();
            if (_Doc.FileName != "")
            {
                var archivo = new FileInfo(_Doc.FileName);
                 txtArchivo.Text= archivo.Name;


            }
        }
        private void GuardarDoc()
        {
            int a = 0;
            if (_Doc.FileName != null)
            {
                if (_Doc.FileName != "")
                {
                    var archivo = new FileInfo(_Doc.FileName);
                    if (Directory.Exists(_rutaDoc))
                    {
                        /*
                        var obtenerArchivos = Directory.GetFiles(_rutaDoc, "*.pdf");
                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length != 0)
                        {
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                //archivoAnterior.Delete();          Esto evita que se sobreescriban los archivos
                                archivo.CopyTo(_rutaDoc + archivo.Name);
                            }
                        }
                        else
                        {*/
                       
                            archivo.CopyTo(_rutaDoc + archivo.Name);
                       

                           
                       
                           
                       // }
                    }
                    else
                    {
                        Directory.CreateDirectory(_rutaDoc);
                        archivo.CopyTo(_rutaDoc + archivo.Name);
                    }
                }
            }
        }
        private void EliminarDoc()
        {
            if (MessageBox.Show("Esta seguro de borrar el archivo PDF", "Eliminar Documento PDF", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_rutaDoc))
                {
                    //
                    var obtenerArchivos = Directory.GetFiles(_rutaDoc, "*.pdf");
                    FileInfo archivoAnterior;
                    if (obtenerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                            txtArchivo.Text = "";

                        }

                    }

                }
            }

        }
        private void BuscarMaestros(string filtro)
        {
            dtgMaestros.DataSource = _maestroManejador.GetMaestros(filtro);
        }
        private void ControlarBotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnCancelar.Enabled = cancelar;

        }
        private void ControlarTexto(bool uno)
        {
            txtApm.Enabled = uno;
            txtApp.Enabled = uno;           
            txtCedula.Enabled = uno;          
            txtDireccion.Enabled = uno;
            txtNombre.Enabled = uno;         
            txtTitulo.Enabled = uno;
            lblClave.Enabled = uno;
            cmbCiudad.Enabled = uno;
            cmbEstado.Enabled = uno;           
            dtpIngreso.Enabled = uno;
            dtpNacimiento.Enabled = uno;

        }
        
        public void Ciudades(string x)
        {
            cmbInvisible.Visible = false;
            cmbInvisible.DataSource = _estadoManejador.CodigoEstado(x);
            cmbInvisible.DisplayMember = "CodigoEstado";
        }
        private void Maestros_Load(object sender, EventArgs e)
        {
           // txtClave.Enabled = false;
            cmbEstado.DataSource = _estadoManejador.GetEstadosList();
            cmbEstado.DisplayMember = "nombre";
            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            BuscarMaestros("");
            lblClave.Text = "0";
            lblID.Text = "0";
            txtClave.ReadOnly = true;
            txtClaveMaestro.ReadOnly = true;
            dtgEstudios.DataSource = _estudioManejador.GetEstudios("");




        }
        private void EliminarUsuario()
        {
            var ID = dtgMaestros.CurrentRow.Cells["Clave"].Value;
            _maestroManejador.Eliminar(ID.ToString());
        }


        private void GuardarMaestros()
        {
            string x = "";
            int a = dtpIngreso.Value.Year;
            int b = dtpIngreso.Value.Month;
            int c = dtpIngreso.Value.Day;
            string d = a.ToString() + "/" + b.ToString() + "/" + c.ToString();

            int t = dtpNacimiento.Value.Year;
            int r = dtpNacimiento.Value.Month;
            int e = dtpNacimiento.Value.Day;
            string q = t.ToString() + "/" + r.ToString() + "/" + e.ToString();

            try
            {
               
                    _maestroManejador.Guardar(new Entidades.ControlEscolar2.Maestros
                    {

                        //ID = int.Parse(lblID.Text),
                        NumeroControl = txtClave.Text,
                        Nombre = txtNombre.Text,
                        App = txtApp.Text,
                        Apm = txtApm.Text,
                        Direccion = txtDireccion.Text,
                        Ciudad = cmbCiudad.Text,
                        Estado = cmbEstado.Text,
                        Cedula = Convert.ToInt32(txtCedula.Text),
                        Titulo = txtTitulo.Text,
                        FechaIngreso = d,
                        FechNacimiento = q,
                        Estudios = cmbEstudios.Text

                    }, x = lblClave.Text);
            }
            catch
            {
                MessageBox.Show("Datos con formato incorrecto");
            }

        }

        public void GuardarEstudios()
        {
            DataSet ds;
            ds = _estudioManejador.IDMaestro(txtClaveMaestro.Text);
            if (ds.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("No se encuentra al profe");
            }
            else
            {
                string vic = ds.Tables[0].Rows[0]["ID"].ToString();               
                _estudioManejador.Guardar(new Entidades.ControlEscolar2.Estudios
                {

                    ID = txtClaveMaestro.Text,
                    Nombre = cmbEstudios.Text,
                    Area = txtArea.Text,
                    Documento = txtArchivo.Text,
                    FkMaestro = int.Parse(vic),
                    Periodo = txtPeriodo.Text


                }, lblEstudios.Text);
            }
            dtgEstudios.DataSource = _estudioManejador.GetEstudios("");
        }
        public  void BorrarA(String archivo)
        {
            if (File.Exists(@archivo))
            {
                try
                {
                   File.Delete(@archivo);
                }
                catch (IOException e)
                {
                  
                }
            }
        }


        private void ModificarAlumnos()
        {

            ControlarBotones(false, true, true, false);
            ControlarTexto(true);


            var NumeroControl = dtgMaestros.CurrentRow.Cells["NumeroControl"].Value;
            lblClave.Text = NumeroControl.ToString();
            txtClave.Text = lblClave.Text;
            txtNombre.Text = dtgMaestros.CurrentRow.Cells["nombre"].Value.ToString();
            txtApp.Text = dtgMaestros.CurrentRow.Cells["app"].Value.ToString();
            txtApm.Text = dtgMaestros.CurrentRow.Cells["apm"].Value.ToString();
            txtDireccion.Text = dtgMaestros.CurrentRow.Cells["direccion"].Value.ToString();
            txtTitulo.Text = dtgMaestros.CurrentRow.Cells["titulo"].Value.ToString();
            txtCedula.Text = dtgMaestros.CurrentRow.Cells["cedula"].Value.ToString();
            cmbEstado.Text = dtgMaestros.CurrentRow.Cells["estado"].Value.ToString();
            cmbCiudad.Text = dtgMaestros.CurrentRow.Cells["ciudad"].Value.ToString();
            txtClaveMaestro.Text = lblClave.Text;
            
        }
        private void ModificarEstudios()
        {
            var NumeroControl = dtgEstudios.CurrentRow.Cells["ID"].Value;          
            lblEstudios.Text = NumeroControl.ToString();
            txtClaveMaestro.Text = lblEstudios.Text;
            txtArea.Text = dtgEstudios.CurrentRow.Cells["area"].Value.ToString();
            txtPeriodo.Text= dtgEstudios.CurrentRow.Cells["periodo"].Value.ToString();
            txtArchivo.Text = dtgEstudios.CurrentRow.Cells["Documento"].Value.ToString();
           lblEstudios.Text= dtgEstudios.CurrentRow.Cells["PK"].Value.ToString();

        }
        void Limpiar()
        {
            txtApm.Clear();
            txtApp.Clear();
            txtArea.Clear();
            txtCedula.Clear();
           // txtClave.Clear();
            txtDireccion.Clear();
            txtNombre.Clear();
            txtPeriodo.Clear();
            txtTitulo.Clear();
            lblClave.Text = "0";
            lblID.Text="0";
            
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            GuardarMaestros();
            if (rbtSi.Checked)
            {
                button1.Enabled = true;
                btnCancelarEstudio.Enabled = true;
                btnEliminarEstudio.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
                btnCancelarEstudio.Enabled = false;
                btnEliminarEstudio.Enabled = false;
            }
            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            Limpiar();
            BuscarMaestros("");
        }

        private void CmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            Ciudades(cmbEstado.Text);
            cmbCiudad.DataSource = _municipioManejador.GetCiudadesList(cmbInvisible.Text);
            cmbCiudad.DisplayMember = "nombreMun";
        }

        private void RbtSi_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void RbtNo_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarTexto(true);
           
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            Limpiar();
            txtClave.Clear();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro de borrar registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    var ID = dtgMaestros.CurrentRow.Cells["NumeroControl"].Value.ToString();
                    _maestroManejador.Eliminar(ID);
                    _estudioManejador.EliminarEnMaestro(ID);
                    dtgEstudios.DataSource = _estudioManejador.GetEstudios("");
                    BuscarMaestros("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show("No se puede borrar");
                }
            }
        }

        private void DtgMaestros_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarAlumnos();          
            txtClave.Enabled = false;
        }

        private void DtpIngreso_ValueChanged(object sender, EventArgs e)
        {
            int id = 0;
            int year = dtpIngreso.Value.Year;          
            DataSet DS;
            DS = _maestroManejador.MaxID(year.ToString());
            if (DS.Tables[0].Rows.Count==0)
            {
                id++;
                txtClave.Text = "D" + year.ToString() + "0" + id.ToString();
                lblID.Text = id.ToString();
                txtClaveMaestro.Text= "D" + year.ToString() + "0" + id.ToString();
                
            }
            else
            {
                string c = DS.Tables[0].Rows[0]["NumControl"].ToString();
                c = c.Substring(5);
                int v = int.Parse(c) + 1;
                lblID.Text = v.ToString();
                if (v<10)
                {
                    txtClave.Text = "D" + year.ToString() + "0" + v.ToString();
                    txtClaveMaestro.Text = "D" + year.ToString() + "0" + v.ToString();
                }
                else
                {
                    txtClave.Text = "D" + year.ToString() + v.ToString();
                    txtClaveMaestro.Text = "D" + year.ToString() + v.ToString();
                }
           
                lblID.Text = v.ToString();
                Limpiar();
         
            }
          
            
        }

        private void TabMaestros_Click(object sender, EventArgs e)
        {

        }

        private void TabEstudios_Click(object sender, EventArgs e)
        {

        }

        private void BtnCargar_Click(object sender, EventArgs e)
        {
            CargarDoc();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            DataSet DS;
            if (txtClaveMaestro.Text == "")
            {
                MessageBox.Show("No hay maestro seleccionado");
                
            }
            else
            {
                DS = _estudioManejador.IDMaestro(txtClaveMaestro.Text);//Este metodo devuelve un Dataset/*
                if (DS.Tables[0].Rows.Count == 0)
                {
                    MessageBox.Show("El maestro no está registrado en el sistema");
                }
                else
                {

                    try
                    {
                        GuardarEstudios();
                        GuardarDoc();
                        txtClaveMaestro.Clear();
                        lblEstudios.Text = "0";
                    }
                    catch 
                    {

                        MessageBox.Show("El archivo ya existe, porfavor cambie el nombre");
                    }
                   
                    dtgEstudios.DataSource = _estudioManejador.GetEstudios("");
                  

                }
            }

        }

        private void BtnEliminarEstudio_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro de borrar registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                string tor = dtgEstudios.CurrentRow.Cells["Documento"].Value.ToString(); // Hay que poner esto antes de eliminar si no da null
                var ID = dtgEstudios.CurrentRow.Cells["PK"].Value.ToString();                   
                    _estudioManejador.Eliminar(int.Parse(ID));
              
                    dtgEstudios.DataSource = _estudioManejador.GetEstudios("");
                               
                BorrarA(_rutaDoc + tor);
               
            }
        }

        private void BtnCancelarEstudio_Click(object sender, EventArgs e)
        {
            txtPeriodo.Clear();
            txtArea.Clear();
            txtClaveMaestro.Clear();
            txtArchivo.Clear();

        }

        private void BtnEliminarDoc_Click(object sender, EventArgs e)
        {
            EliminarDoc();
        }

        private void DtgEstudios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
            ModificarEstudios();
        }

        private void DtgMaestros_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
