﻿namespace ControlEscolar2
{
    partial class Grupos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Grupos));
            this.dtgGrupo = new System.Windows.Forms.DataGridView();
            this.gpoGrupo = new System.Windows.Forms.GroupBox();
            this.lblIDGrupo = new System.Windows.Forms.Label();
            this.cmbCarrera = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbTurno = new System.Windows.Forms.ComboBox();
            this.cmbSemestre = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNombreGrupo = new System.Windows.Forms.TextBox();
            this.tbcAsignacion = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnEliminarGrupo = new System.Windows.Forms.Button();
            this.btnCancelarGrupo = new System.Windows.Forms.Button();
            this.btnGuardarGrupo = new System.Windows.Forms.Button();
            this.tabGrupo_Alumnos = new System.Windows.Forms.TabPage();
            this.cmbSeleccionarGrupo = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lblIDasignacion = new System.Windows.Forms.Label();
            this.lblIDAlumno = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dtgAsigancion_Alumnos = new System.Windows.Forms.DataGridView();
            this.btnEliminarGrupo_Alumnos = new System.Windows.Forms.Button();
            this.btnCancelarGrupo_Alumnos = new System.Windows.Forms.Button();
            this.btnGuardarGrupo_Alumnos = new System.Windows.Forms.Button();
            this.dtgAlumnos = new System.Windows.Forms.DataGridView();
            this.cmbClaveGrupo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNombreAlumno = new System.Windows.Forms.TextBox();
            this.txtAppAlumno = new System.Windows.Forms.TextBox();
            this.txtApmAlumno = new System.Windows.Forms.TextBox();
            this.txtDomicilioAlumno = new System.Windows.Forms.TextBox();
            this.txtEstadoAlumno = new System.Windows.Forms.TextBox();
            this.txtMunicipioAlumno = new System.Windows.Forms.TextBox();
            this.txtEmailAlumno = new System.Windows.Forms.TextBox();
            this.txtNumControl = new System.Windows.Forms.TextBox();
            this.tabMaestros_Materias = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.btnEliminarMaestro_Grupo = new System.Windows.Forms.Button();
            this.btnCancelarMaestro_Grupo = new System.Windows.Forms.Button();
            this.btnGuardarMaestro_Grupo = new System.Windows.Forms.Button();
            this.dtgMaestros_Grupos = new System.Windows.Forms.DataGridView();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbClaveMateria = new System.Windows.Forms.ComboBox();
            this.cmbClaveGrupos = new System.Windows.Forms.ComboBox();
            this.cmbClaveMaestro = new System.Windows.Forms.ComboBox();
            this.txtClaveAsignacion = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgGrupo)).BeginInit();
            this.gpoGrupo.SuspendLayout();
            this.tbcAsignacion.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabGrupo_Alumnos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAsigancion_Alumnos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).BeginInit();
            this.tabMaestros_Materias.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgMaestros_Grupos)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgGrupo
            // 
            this.dtgGrupo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgGrupo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtgGrupo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgGrupo.Location = new System.Drawing.Point(24, 119);
            this.dtgGrupo.Name = "dtgGrupo";
            this.dtgGrupo.Size = new System.Drawing.Size(672, 148);
            this.dtgGrupo.TabIndex = 0;
            this.dtgGrupo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgGrupo_CellClick);
            this.dtgGrupo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgGrupo_CellDoubleClick);
            // 
            // gpoGrupo
            // 
            this.gpoGrupo.Controls.Add(this.lblIDGrupo);
            this.gpoGrupo.Controls.Add(this.cmbCarrera);
            this.gpoGrupo.Controls.Add(this.label4);
            this.gpoGrupo.Controls.Add(this.cmbTurno);
            this.gpoGrupo.Controls.Add(this.cmbSemestre);
            this.gpoGrupo.Controls.Add(this.label3);
            this.gpoGrupo.Controls.Add(this.label2);
            this.gpoGrupo.Controls.Add(this.label1);
            this.gpoGrupo.Controls.Add(this.txtNombreGrupo);
            this.gpoGrupo.Location = new System.Drawing.Point(24, 6);
            this.gpoGrupo.Name = "gpoGrupo";
            this.gpoGrupo.Size = new System.Drawing.Size(672, 94);
            this.gpoGrupo.TabIndex = 1;
            this.gpoGrupo.TabStop = false;
            this.gpoGrupo.Text = "Datos del grupo";
            // 
            // lblIDGrupo
            // 
            this.lblIDGrupo.AutoSize = true;
            this.lblIDGrupo.Location = new System.Drawing.Point(628, 20);
            this.lblIDGrupo.Name = "lblIDGrupo";
            this.lblIDGrupo.Size = new System.Drawing.Size(13, 13);
            this.lblIDGrupo.TabIndex = 10;
            this.lblIDGrupo.Text = "0";
            // 
            // cmbCarrera
            // 
            this.cmbCarrera.FormattingEnabled = true;
            this.cmbCarrera.Items.AddRange(new object[] {
            "Sistemas Computacionales",
            "Gestion Empresarial",
            "Industrial",
            "Electromecanica",
            "Civil",
            "Automotriz"});
            this.cmbCarrera.Location = new System.Drawing.Point(218, 53);
            this.cmbCarrera.Name = "cmbCarrera";
            this.cmbCarrera.Size = new System.Drawing.Size(217, 21);
            this.cmbCarrera.TabIndex = 9;
            this.cmbCarrera.SelectedIndexChanged += new System.EventHandler(this.CmbCarrera_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(540, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Turno";
            // 
            // cmbTurno
            // 
            this.cmbTurno.FormattingEnabled = true;
            this.cmbTurno.Items.AddRange(new object[] {
            "Matutino",
            "Vespertino"});
            this.cmbTurno.Location = new System.Drawing.Point(543, 54);
            this.cmbTurno.Name = "cmbTurno";
            this.cmbTurno.Size = new System.Drawing.Size(109, 21);
            this.cmbTurno.TabIndex = 7;
            this.cmbTurno.SelectedIndexChanged += new System.EventHandler(this.CmbTurno_SelectedIndexChanged);
            // 
            // cmbSemestre
            // 
            this.cmbSemestre.FormattingEnabled = true;
            this.cmbSemestre.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.cmbSemestre.Location = new System.Drawing.Point(441, 53);
            this.cmbSemestre.Name = "cmbSemestre";
            this.cmbSemestre.Size = new System.Drawing.Size(96, 21);
            this.cmbSemestre.TabIndex = 6;
            this.cmbSemestre.SelectedIndexChanged += new System.EventHandler(this.CmbSemestre_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(215, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Carrera";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(438, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Semestre";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Clave del grupo";
            // 
            // txtNombreGrupo
            // 
            this.txtNombreGrupo.Location = new System.Drawing.Point(5, 54);
            this.txtNombreGrupo.Name = "txtNombreGrupo";
            this.txtNombreGrupo.ReadOnly = true;
            this.txtNombreGrupo.Size = new System.Drawing.Size(207, 20);
            this.txtNombreGrupo.TabIndex = 0;
            // 
            // tbcAsignacion
            // 
            this.tbcAsignacion.Controls.Add(this.tabPage2);
            this.tbcAsignacion.Controls.Add(this.tabGrupo_Alumnos);
            this.tbcAsignacion.Controls.Add(this.tabMaestros_Materias);
            this.tbcAsignacion.Location = new System.Drawing.Point(0, -1);
            this.tbcAsignacion.Name = "tbcAsignacion";
            this.tbcAsignacion.SelectedIndex = 0;
            this.tbcAsignacion.Size = new System.Drawing.Size(714, 491);
            this.tbcAsignacion.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage2.Controls.Add(this.btnEliminarGrupo);
            this.tabPage2.Controls.Add(this.btnCancelarGrupo);
            this.tabPage2.Controls.Add(this.btnGuardarGrupo);
            this.tabPage2.Controls.Add(this.gpoGrupo);
            this.tabPage2.Controls.Add(this.dtgGrupo);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(706, 465);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Creación de grupos";
            this.tabPage2.Click += new System.EventHandler(this.TabPage2_Click);
            // 
            // btnEliminarGrupo
            // 
            this.btnEliminarGrupo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminarGrupo.BackgroundImage")));
            this.btnEliminarGrupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminarGrupo.Location = new System.Drawing.Point(632, 369);
            this.btnEliminarGrupo.Name = "btnEliminarGrupo";
            this.btnEliminarGrupo.Size = new System.Drawing.Size(64, 67);
            this.btnEliminarGrupo.TabIndex = 30;
            this.btnEliminarGrupo.UseVisualStyleBackColor = true;
            this.btnEliminarGrupo.Click += new System.EventHandler(this.BtnEliminarGrupo_Click);
            // 
            // btnCancelarGrupo
            // 
            this.btnCancelarGrupo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelarGrupo.BackgroundImage")));
            this.btnCancelarGrupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelarGrupo.Location = new System.Drawing.Point(562, 369);
            this.btnCancelarGrupo.Name = "btnCancelarGrupo";
            this.btnCancelarGrupo.Size = new System.Drawing.Size(64, 67);
            this.btnCancelarGrupo.TabIndex = 29;
            this.btnCancelarGrupo.UseVisualStyleBackColor = true;
            this.btnCancelarGrupo.Click += new System.EventHandler(this.BtnCancelarGrupo_Click);
            // 
            // btnGuardarGrupo
            // 
            this.btnGuardarGrupo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardarGrupo.BackgroundImage")));
            this.btnGuardarGrupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardarGrupo.Location = new System.Drawing.Point(487, 369);
            this.btnGuardarGrupo.Name = "btnGuardarGrupo";
            this.btnGuardarGrupo.Size = new System.Drawing.Size(69, 67);
            this.btnGuardarGrupo.TabIndex = 28;
            this.btnGuardarGrupo.UseVisualStyleBackColor = true;
            this.btnGuardarGrupo.Click += new System.EventHandler(this.BtnGuardarGrupo_Click);
            // 
            // tabGrupo_Alumnos
            // 
            this.tabGrupo_Alumnos.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabGrupo_Alumnos.Controls.Add(this.cmbSeleccionarGrupo);
            this.tabGrupo_Alumnos.Controls.Add(this.comboBox1);
            this.tabGrupo_Alumnos.Controls.Add(this.lblIDasignacion);
            this.tabGrupo_Alumnos.Controls.Add(this.lblIDAlumno);
            this.tabGrupo_Alumnos.Controls.Add(this.label13);
            this.tabGrupo_Alumnos.Controls.Add(this.dtgAsigancion_Alumnos);
            this.tabGrupo_Alumnos.Controls.Add(this.btnEliminarGrupo_Alumnos);
            this.tabGrupo_Alumnos.Controls.Add(this.btnCancelarGrupo_Alumnos);
            this.tabGrupo_Alumnos.Controls.Add(this.btnGuardarGrupo_Alumnos);
            this.tabGrupo_Alumnos.Controls.Add(this.dtgAlumnos);
            this.tabGrupo_Alumnos.Controls.Add(this.cmbClaveGrupo);
            this.tabGrupo_Alumnos.Controls.Add(this.label14);
            this.tabGrupo_Alumnos.Controls.Add(this.label12);
            this.tabGrupo_Alumnos.Controls.Add(this.label11);
            this.tabGrupo_Alumnos.Controls.Add(this.label10);
            this.tabGrupo_Alumnos.Controls.Add(this.label9);
            this.tabGrupo_Alumnos.Controls.Add(this.label8);
            this.tabGrupo_Alumnos.Controls.Add(this.label7);
            this.tabGrupo_Alumnos.Controls.Add(this.label6);
            this.tabGrupo_Alumnos.Controls.Add(this.label5);
            this.tabGrupo_Alumnos.Controls.Add(this.txtNombreAlumno);
            this.tabGrupo_Alumnos.Controls.Add(this.txtAppAlumno);
            this.tabGrupo_Alumnos.Controls.Add(this.txtApmAlumno);
            this.tabGrupo_Alumnos.Controls.Add(this.txtDomicilioAlumno);
            this.tabGrupo_Alumnos.Controls.Add(this.txtEstadoAlumno);
            this.tabGrupo_Alumnos.Controls.Add(this.txtMunicipioAlumno);
            this.tabGrupo_Alumnos.Controls.Add(this.txtEmailAlumno);
            this.tabGrupo_Alumnos.Controls.Add(this.txtNumControl);
            this.tabGrupo_Alumnos.Location = new System.Drawing.Point(4, 22);
            this.tabGrupo_Alumnos.Name = "tabGrupo_Alumnos";
            this.tabGrupo_Alumnos.Padding = new System.Windows.Forms.Padding(3);
            this.tabGrupo_Alumnos.Size = new System.Drawing.Size(706, 465);
            this.tabGrupo_Alumnos.TabIndex = 2;
            this.tabGrupo_Alumnos.Text = "Asignación de alumnos";
            // 
            // cmbSeleccionarGrupo
            // 
            this.cmbSeleccionarGrupo.AutoSize = true;
            this.cmbSeleccionarGrupo.Location = new System.Drawing.Point(465, 255);
            this.cmbSeleccionarGrupo.Name = "cmbSeleccionarGrupo";
            this.cmbSeleccionarGrupo.Size = new System.Drawing.Size(108, 13);
            this.cmbSeleccionarGrupo.TabIndex = 32;
            this.cmbSeleccionarGrupo.Text = "Selecciona un grupo:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(573, 252);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 31;
            this.comboBox1.TextChanged += new System.EventHandler(this.ComboBox1_TextChanged);
            // 
            // lblIDasignacion
            // 
            this.lblIDasignacion.AutoSize = true;
            this.lblIDasignacion.Location = new System.Drawing.Point(655, 35);
            this.lblIDasignacion.Name = "lblIDasignacion";
            this.lblIDasignacion.Size = new System.Drawing.Size(13, 13);
            this.lblIDasignacion.TabIndex = 30;
            this.lblIDasignacion.Text = "0";
            // 
            // lblIDAlumno
            // 
            this.lblIDAlumno.AutoSize = true;
            this.lblIDAlumno.Location = new System.Drawing.Point(655, 7);
            this.lblIDAlumno.Name = "lblIDAlumno";
            this.lblIDAlumno.Size = new System.Drawing.Size(13, 13);
            this.lblIDAlumno.TabIndex = 29;
            this.lblIDAlumno.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 255);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Alumnos por grupo";
            // 
            // dtgAsigancion_Alumnos
            // 
            this.dtgAsigancion_Alumnos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgAsigancion_Alumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgAsigancion_Alumnos.Location = new System.Drawing.Point(6, 283);
            this.dtgAsigancion_Alumnos.Name = "dtgAsigancion_Alumnos";
            this.dtgAsigancion_Alumnos.Size = new System.Drawing.Size(688, 100);
            this.dtgAsigancion_Alumnos.TabIndex = 27;
            this.dtgAsigancion_Alumnos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgAsigancion_Alumnos_CellClick);
            this.dtgAsigancion_Alumnos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgAsigancion_Alumnos_CellContentClick);
            this.dtgAsigancion_Alumnos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgAsigancion_Alumnos_CellDoubleClick);
            // 
            // btnEliminarGrupo_Alumnos
            // 
            this.btnEliminarGrupo_Alumnos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminarGrupo_Alumnos.BackgroundImage")));
            this.btnEliminarGrupo_Alumnos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminarGrupo_Alumnos.Location = new System.Drawing.Point(631, 398);
            this.btnEliminarGrupo_Alumnos.Name = "btnEliminarGrupo_Alumnos";
            this.btnEliminarGrupo_Alumnos.Size = new System.Drawing.Size(63, 61);
            this.btnEliminarGrupo_Alumnos.TabIndex = 26;
            this.btnEliminarGrupo_Alumnos.UseVisualStyleBackColor = true;
            this.btnEliminarGrupo_Alumnos.Click += new System.EventHandler(this.BtnEliminarGrupo_Alumnos_Click);
            // 
            // btnCancelarGrupo_Alumnos
            // 
            this.btnCancelarGrupo_Alumnos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelarGrupo_Alumnos.BackgroundImage")));
            this.btnCancelarGrupo_Alumnos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelarGrupo_Alumnos.Location = new System.Drawing.Point(565, 398);
            this.btnCancelarGrupo_Alumnos.Name = "btnCancelarGrupo_Alumnos";
            this.btnCancelarGrupo_Alumnos.Size = new System.Drawing.Size(60, 61);
            this.btnCancelarGrupo_Alumnos.TabIndex = 25;
            this.btnCancelarGrupo_Alumnos.UseVisualStyleBackColor = true;
            this.btnCancelarGrupo_Alumnos.Click += new System.EventHandler(this.BtnCancelarGrupo_Alumnos_Click);
            // 
            // btnGuardarGrupo_Alumnos
            // 
            this.btnGuardarGrupo_Alumnos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardarGrupo_Alumnos.BackgroundImage")));
            this.btnGuardarGrupo_Alumnos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardarGrupo_Alumnos.Location = new System.Drawing.Point(494, 398);
            this.btnGuardarGrupo_Alumnos.Name = "btnGuardarGrupo_Alumnos";
            this.btnGuardarGrupo_Alumnos.Size = new System.Drawing.Size(65, 61);
            this.btnGuardarGrupo_Alumnos.TabIndex = 24;
            this.btnGuardarGrupo_Alumnos.UseVisualStyleBackColor = true;
            this.btnGuardarGrupo_Alumnos.Click += new System.EventHandler(this.BtnGuardarGrupo_Alumnos_Click);
            // 
            // dtgAlumnos
            // 
            this.dtgAlumnos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dtgAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgAlumnos.Location = new System.Drawing.Point(6, 143);
            this.dtgAlumnos.Name = "dtgAlumnos";
            this.dtgAlumnos.Size = new System.Drawing.Size(688, 97);
            this.dtgAlumnos.TabIndex = 22;
            this.dtgAlumnos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgAlumnos_CellDoubleClick);
            // 
            // cmbClaveGrupo
            // 
            this.cmbClaveGrupo.FormattingEnabled = true;
            this.cmbClaveGrupo.Location = new System.Drawing.Point(243, 27);
            this.cmbClaveGrupo.Name = "cmbClaveGrupo";
            this.cmbClaveGrupo.Size = new System.Drawing.Size(158, 21);
            this.cmbClaveGrupo.TabIndex = 21;
            this.cmbClaveGrupo.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            this.cmbClaveGrupo.TextChanged += new System.EventHandler(this.CmbClaveGrupo_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(240, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "Clave del grupo";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(382, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Apellido materno";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 96);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Domicilio";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(505, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Correo electrónico";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(244, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Apellido paterno";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Nombre";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(244, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Estado";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(408, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Municipio";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Numero de Control";
            // 
            // txtNombreAlumno
            // 
            this.txtNombreAlumno.Location = new System.Drawing.Point(8, 69);
            this.txtNombreAlumno.Name = "txtNombreAlumno";
            this.txtNombreAlumno.ReadOnly = true;
            this.txtNombreAlumno.Size = new System.Drawing.Size(229, 20);
            this.txtNombreAlumno.TabIndex = 8;
            // 
            // txtAppAlumno
            // 
            this.txtAppAlumno.Location = new System.Drawing.Point(243, 69);
            this.txtAppAlumno.Name = "txtAppAlumno";
            this.txtAppAlumno.ReadOnly = true;
            this.txtAppAlumno.Size = new System.Drawing.Size(133, 20);
            this.txtAppAlumno.TabIndex = 7;
            // 
            // txtApmAlumno
            // 
            this.txtApmAlumno.Location = new System.Drawing.Point(382, 69);
            this.txtApmAlumno.Name = "txtApmAlumno";
            this.txtApmAlumno.ReadOnly = true;
            this.txtApmAlumno.Size = new System.Drawing.Size(117, 20);
            this.txtApmAlumno.TabIndex = 6;
            // 
            // txtDomicilioAlumno
            // 
            this.txtDomicilioAlumno.Location = new System.Drawing.Point(8, 112);
            this.txtDomicilioAlumno.Name = "txtDomicilioAlumno";
            this.txtDomicilioAlumno.ReadOnly = true;
            this.txtDomicilioAlumno.Size = new System.Drawing.Size(229, 20);
            this.txtDomicilioAlumno.TabIndex = 5;
            // 
            // txtEstadoAlumno
            // 
            this.txtEstadoAlumno.Location = new System.Drawing.Point(243, 112);
            this.txtEstadoAlumno.Name = "txtEstadoAlumno";
            this.txtEstadoAlumno.ReadOnly = true;
            this.txtEstadoAlumno.Size = new System.Drawing.Size(162, 20);
            this.txtEstadoAlumno.TabIndex = 4;
            // 
            // txtMunicipioAlumno
            // 
            this.txtMunicipioAlumno.Location = new System.Drawing.Point(411, 112);
            this.txtMunicipioAlumno.Name = "txtMunicipioAlumno";
            this.txtMunicipioAlumno.ReadOnly = true;
            this.txtMunicipioAlumno.Size = new System.Drawing.Size(159, 20);
            this.txtMunicipioAlumno.TabIndex = 2;
            // 
            // txtEmailAlumno
            // 
            this.txtEmailAlumno.Location = new System.Drawing.Point(505, 69);
            this.txtEmailAlumno.Name = "txtEmailAlumno";
            this.txtEmailAlumno.ReadOnly = true;
            this.txtEmailAlumno.Size = new System.Drawing.Size(189, 20);
            this.txtEmailAlumno.TabIndex = 1;
            // 
            // txtNumControl
            // 
            this.txtNumControl.Location = new System.Drawing.Point(8, 28);
            this.txtNumControl.Name = "txtNumControl";
            this.txtNumControl.ReadOnly = true;
            this.txtNumControl.Size = new System.Drawing.Size(229, 20);
            this.txtNumControl.TabIndex = 0;
            // 
            // tabMaestros_Materias
            // 
            this.tabMaestros_Materias.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabMaestros_Materias.Controls.Add(this.label19);
            this.tabMaestros_Materias.Controls.Add(this.btnEliminarMaestro_Grupo);
            this.tabMaestros_Materias.Controls.Add(this.btnCancelarMaestro_Grupo);
            this.tabMaestros_Materias.Controls.Add(this.btnGuardarMaestro_Grupo);
            this.tabMaestros_Materias.Controls.Add(this.dtgMaestros_Grupos);
            this.tabMaestros_Materias.Controls.Add(this.label18);
            this.tabMaestros_Materias.Controls.Add(this.label17);
            this.tabMaestros_Materias.Controls.Add(this.label16);
            this.tabMaestros_Materias.Controls.Add(this.label15);
            this.tabMaestros_Materias.Controls.Add(this.cmbClaveMateria);
            this.tabMaestros_Materias.Controls.Add(this.cmbClaveGrupos);
            this.tabMaestros_Materias.Controls.Add(this.cmbClaveMaestro);
            this.tabMaestros_Materias.Controls.Add(this.txtClaveAsignacion);
            this.tabMaestros_Materias.Location = new System.Drawing.Point(4, 22);
            this.tabMaestros_Materias.Name = "tabMaestros_Materias";
            this.tabMaestros_Materias.Padding = new System.Windows.Forms.Padding(3);
            this.tabMaestros_Materias.Size = new System.Drawing.Size(706, 465);
            this.tabMaestros_Materias.TabIndex = 3;
            this.tabMaestros_Materias.Text = "Asignacion de maestros";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(652, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "0";
            // 
            // btnEliminarMaestro_Grupo
            // 
            this.btnEliminarMaestro_Grupo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminarMaestro_Grupo.BackgroundImage")));
            this.btnEliminarMaestro_Grupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminarMaestro_Grupo.Location = new System.Drawing.Point(630, 395);
            this.btnEliminarMaestro_Grupo.Name = "btnEliminarMaestro_Grupo";
            this.btnEliminarMaestro_Grupo.Size = new System.Drawing.Size(64, 64);
            this.btnEliminarMaestro_Grupo.TabIndex = 34;
            this.btnEliminarMaestro_Grupo.UseVisualStyleBackColor = true;
            this.btnEliminarMaestro_Grupo.Click += new System.EventHandler(this.BtnEliminarMaestro_Grupo_Click);
            // 
            // btnCancelarMaestro_Grupo
            // 
            this.btnCancelarMaestro_Grupo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelarMaestro_Grupo.BackgroundImage")));
            this.btnCancelarMaestro_Grupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelarMaestro_Grupo.Location = new System.Drawing.Point(560, 395);
            this.btnCancelarMaestro_Grupo.Name = "btnCancelarMaestro_Grupo";
            this.btnCancelarMaestro_Grupo.Size = new System.Drawing.Size(64, 64);
            this.btnCancelarMaestro_Grupo.TabIndex = 33;
            this.btnCancelarMaestro_Grupo.UseVisualStyleBackColor = true;
            // 
            // btnGuardarMaestro_Grupo
            // 
            this.btnGuardarMaestro_Grupo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardarMaestro_Grupo.BackgroundImage")));
            this.btnGuardarMaestro_Grupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGuardarMaestro_Grupo.Location = new System.Drawing.Point(489, 395);
            this.btnGuardarMaestro_Grupo.Name = "btnGuardarMaestro_Grupo";
            this.btnGuardarMaestro_Grupo.Size = new System.Drawing.Size(65, 64);
            this.btnGuardarMaestro_Grupo.TabIndex = 32;
            this.btnGuardarMaestro_Grupo.UseVisualStyleBackColor = true;
            this.btnGuardarMaestro_Grupo.Click += new System.EventHandler(this.BtnGuardarMaestro_Grupo_Click);
            // 
            // dtgMaestros_Grupos
            // 
            this.dtgMaestros_Grupos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgMaestros_Grupos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtgMaestros_Grupos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgMaestros_Grupos.Location = new System.Drawing.Point(9, 125);
            this.dtgMaestros_Grupos.Name = "dtgMaestros_Grupos";
            this.dtgMaestros_Grupos.Size = new System.Drawing.Size(685, 198);
            this.dtgMaestros_Grupos.TabIndex = 8;
            this.dtgMaestros_Grupos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgMaestros_Grupos_CellClick);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(520, 32);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "Clave de la materia";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(343, 32);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Clave del grupo";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(166, 32);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Clave del maestro";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(114, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Clave de la asignación";
            // 
            // cmbClaveMateria
            // 
            this.cmbClaveMateria.FormattingEnabled = true;
            this.cmbClaveMateria.Location = new System.Drawing.Point(523, 48);
            this.cmbClaveMateria.Name = "cmbClaveMateria";
            this.cmbClaveMateria.Size = new System.Drawing.Size(171, 21);
            this.cmbClaveMateria.TabIndex = 3;
            this.cmbClaveMateria.SelectedIndexChanged += new System.EventHandler(this.CmbClaveMateria_SelectedIndexChanged);
            // 
            // cmbClaveGrupos
            // 
            this.cmbClaveGrupos.FormattingEnabled = true;
            this.cmbClaveGrupos.Location = new System.Drawing.Point(346, 48);
            this.cmbClaveGrupos.Name = "cmbClaveGrupos";
            this.cmbClaveGrupos.Size = new System.Drawing.Size(171, 21);
            this.cmbClaveGrupos.TabIndex = 2;
            this.cmbClaveGrupos.SelectedIndexChanged += new System.EventHandler(this.CmbClaveGrupos_SelectedIndexChanged);
            // 
            // cmbClaveMaestro
            // 
            this.cmbClaveMaestro.FormattingEnabled = true;
            this.cmbClaveMaestro.Location = new System.Drawing.Point(169, 48);
            this.cmbClaveMaestro.Name = "cmbClaveMaestro";
            this.cmbClaveMaestro.Size = new System.Drawing.Size(171, 21);
            this.cmbClaveMaestro.TabIndex = 1;
            this.cmbClaveMaestro.SelectedIndexChanged += new System.EventHandler(this.CmbClaveMaestro_SelectedIndexChanged);
            // 
            // txtClaveAsignacion
            // 
            this.txtClaveAsignacion.Location = new System.Drawing.Point(9, 48);
            this.txtClaveAsignacion.Name = "txtClaveAsignacion";
            this.txtClaveAsignacion.ReadOnly = true;
            this.txtClaveAsignacion.Size = new System.Drawing.Size(153, 20);
            this.txtClaveAsignacion.TabIndex = 0;
            // 
            // Grupos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 496);
            this.Controls.Add(this.tbcAsignacion);
            this.Name = "Grupos";
            this.Text = "Grupos";
            this.Load += new System.EventHandler(this.Grupos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgGrupo)).EndInit();
            this.gpoGrupo.ResumeLayout(false);
            this.gpoGrupo.PerformLayout();
            this.tbcAsignacion.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabGrupo_Alumnos.ResumeLayout(false);
            this.tabGrupo_Alumnos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAsigancion_Alumnos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).EndInit();
            this.tabMaestros_Materias.ResumeLayout(false);
            this.tabMaestros_Materias.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgMaestros_Grupos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgGrupo;
        private System.Windows.Forms.GroupBox gpoGrupo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNombreGrupo;
        private System.Windows.Forms.ComboBox cmbSemestre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbTurno;
        private System.Windows.Forms.TabControl tbcAsignacion;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabGrupo_Alumnos;
        private System.Windows.Forms.TextBox txtNombreAlumno;
        private System.Windows.Forms.TextBox txtAppAlumno;
        private System.Windows.Forms.TextBox txtApmAlumno;
        private System.Windows.Forms.TextBox txtDomicilioAlumno;
        private System.Windows.Forms.TextBox txtEstadoAlumno;
        private System.Windows.Forms.TextBox txtMunicipioAlumno;
        private System.Windows.Forms.TextBox txtEmailAlumno;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnEliminarGrupo_Alumnos;
        private System.Windows.Forms.Button btnCancelarGrupo_Alumnos;
        private System.Windows.Forms.Button btnGuardarGrupo_Alumnos;
        private System.Windows.Forms.DataGridView dtgAlumnos;
        private System.Windows.Forms.ComboBox cmbClaveGrupo;
        private System.Windows.Forms.TextBox txtNumControl;
        private System.Windows.Forms.Button btnEliminarGrupo;
        private System.Windows.Forms.Button btnCancelarGrupo;
        private System.Windows.Forms.Button btnGuardarGrupo;
        private System.Windows.Forms.ComboBox cmbCarrera;
        private System.Windows.Forms.Label lblIDGrupo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dtgAsigancion_Alumnos;
        private System.Windows.Forms.Label lblIDasignacion;
        private System.Windows.Forms.Label lblIDAlumno;
        private System.Windows.Forms.Label cmbSeleccionarGrupo;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TabPage tabMaestros_Materias;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbClaveMateria;
        private System.Windows.Forms.ComboBox cmbClaveGrupos;
        private System.Windows.Forms.ComboBox cmbClaveMaestro;
        private System.Windows.Forms.TextBox txtClaveAsignacion;
        private System.Windows.Forms.Button btnEliminarMaestro_Grupo;
        private System.Windows.Forms.Button btnCancelarMaestro_Grupo;
        private System.Windows.Forms.Button btnGuardarMaestro_Grupo;
        private System.Windows.Forms.DataGridView dtgMaestros_Grupos;
        private System.Windows.Forms.Label label19;
    }
}