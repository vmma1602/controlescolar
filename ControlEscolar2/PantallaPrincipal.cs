﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar2
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void MsMenuPrincipal_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
           
        }

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuarios frmUsu = new frmUsuarios();
            frmUsu.ShowDialog();
        }

        private void AlumnosToolStrip_Click(object sender, EventArgs e)
        {
            Alumnos alu = new Alumnos();
            alu.ShowDialog();
        }

        private void MaestrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Maestros ma = new Maestros();
            ma.Show();
        }

        private void MateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Materias materias = new Materias();
            materias.Show();
        }

        private void EscuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Escuela escuela = new Escuela();
            escuela.Show();
        }

        private void GruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Grupos grupos = new Grupos();
            grupos.Show();
        }

        private void CalificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Calificaciones calificaciones = new Calificaciones();
            calificaciones.Show();
        }
    }
}
