﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace ControlEscolar2
{

    public partial class frmUsuarios : Form
    {
        private UsuarioManejador _usuarioManejador;
        public frmUsuarios()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
        }

        private void FrmUsuarios_Load(object sender, EventArgs e)
        {
            BuscarUsuarios("");
            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            Limpiar();
        }
        private void BuscarUsuarios(string filtro)
        {
            dtgUsuarios.DataSource = _usuarioManejador.GetUsuarios(filtro);
        }
        private void ControlarBotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnCancelar.Enabled = cancelar;

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarTexto(true);
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {

            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            try
            {
                GuardarUsuario();
                BuscarUsuarios("");
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void GuardarUsuario()
        {
            _usuarioManejador.Guardar(new Usuario
            {
                Idusuario = int.Parse(lblID.Text),
                Nombre = txtNombre.Text,
                App = txtApp.Text,
                Apm = txtApm.Text,
                Contrasenia = txtContraseña.Text
            });

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarTexto(false);
            Limpiar();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro de borrar registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarUsuario();
                    BuscarUsuarios("");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
        private void EliminarUsuario()
        {
            var IDusuario = dtgUsuarios.CurrentRow.Cells["IDusuario"].Value;
            _usuarioManejador.Eliminar(Convert.ToInt32(IDusuario));
        }

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {
        }
        private void ControlarTexto(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApp.Enabled = activar;
            txtApm.Enabled = activar;
            txtContraseña.Enabled = activar;
        }
        private void Limpiar()
        {
            txtApm.Clear();
            txtApp.Clear();
            txtContraseña.Clear();
            txtNombre.Clear();
            lblID.Text = 0.ToString();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarUsuarios(txtNombre.Text);
        }

        private void DtgUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarUsuarios();
                BuscarUsuarios("");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void ModificarUsuarios()   
            {
           
                ControlarBotones(false, true, true, false);
                ControlarTexto(true);
                var IDusuario = dtgUsuarios.CurrentRow.Cells["IDusuario"].Value;
                lblID.Text = IDusuario.ToString();
                txtApp.Text  = dtgUsuarios.CurrentRow.Cells["app"].Value.ToString();
            txtApm.Text = dtgUsuarios.CurrentRow.Cells["apm"].Value.ToString();
            txtNombre.Text = dtgUsuarios.CurrentRow.Cells["nombre"].Value.ToString();
                txtContraseña.Text = dtgUsuarios.CurrentRow.Cells["contrasenia"].Value.ToString();       


        }

        private void TxtContraseña_TextChanged(object sender, EventArgs e)
        {
            BuscarUsuarios(txtBuscar.Text);
        }
    }
}
