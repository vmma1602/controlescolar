﻿namespace ControlEscolar2
{
    partial class Maestros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Maestros));
            this.label1 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtApp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtApm = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpIngreso = new System.Windows.Forms.DateTimePicker();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbEstado = new System.Windows.Forms.ComboBox();
            this.cmbCiudad = new System.Windows.Forms.ComboBox();
            this.txtClave = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTitulo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCedula = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpNacimiento = new System.Windows.Forms.DateTimePicker();
            this.lblClave = new System.Windows.Forms.Label();
            this.dtgMaestros = new System.Windows.Forms.DataGridView();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbEstudios = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtArea = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPeriodo = new System.Windows.Forms.TextBox();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.cmbInvisible = new System.Windows.Forms.ComboBox();
            this.gpoEstudios = new System.Windows.Forms.GroupBox();
            this.txtArchivo = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtClaveMaestro = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnEliminarDoc = new System.Windows.Forms.Button();
            this.btnCargar = new System.Windows.Forms.Button();
            this.lblID = new System.Windows.Forms.Label();
            this.tbcMaestros = new System.Windows.Forms.TabControl();
            this.tabMaestros = new System.Windows.Forms.TabPage();
            this.rbtNo = new System.Windows.Forms.RadioButton();
            this.rbtSi = new System.Windows.Forms.RadioButton();
            this.label17 = new System.Windows.Forms.Label();
            this.tabEstudios = new System.Windows.Forms.TabPage();
            this.lblEstudios = new System.Windows.Forms.Label();
            this.dtgEstudios = new System.Windows.Forms.DataGridView();
            this.btnEliminarEstudio = new System.Windows.Forms.Button();
            this.btnCancelarEstudio = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgMaestros)).BeginInit();
            this.gpoEstudios.SuspendLayout();
            this.tbcMaestros.SuspendLayout();
            this.tabMaestros.SuspendLayout();
            this.tabEstudios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEstudios)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(10, 31);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(215, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // txtApp
            // 
            this.txtApp.Location = new System.Drawing.Point(240, 31);
            this.txtApp.Name = "txtApp";
            this.txtApp.Size = new System.Drawing.Size(123, 20);
            this.txtApp.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(237, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Apellido paterno";
            // 
            // txtApm
            // 
            this.txtApm.Location = new System.Drawing.Point(369, 31);
            this.txtApm.Name = "txtApm";
            this.txtApm.Size = new System.Drawing.Size(123, 20);
            this.txtApm.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(366, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Apellido materno";
            // 
            // dtpIngreso
            // 
            this.dtpIngreso.Location = new System.Drawing.Point(499, 31);
            this.dtpIngreso.Name = "dtpIngreso";
            this.dtpIngreso.Size = new System.Drawing.Size(226, 20);
            this.dtpIngreso.TabIndex = 6;
            this.dtpIngreso.ValueChanged += new System.EventHandler(this.DtpIngreso_ValueChanged);
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(10, 75);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(353, 20);
            this.txtDireccion.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Dirección";
            // 
            // cmbEstado
            // 
            this.cmbEstado.FormattingEnabled = true;
            this.cmbEstado.Location = new System.Drawing.Point(369, 75);
            this.cmbEstado.Name = "cmbEstado";
            this.cmbEstado.Size = new System.Drawing.Size(181, 21);
            this.cmbEstado.TabIndex = 9;
            this.cmbEstado.SelectedIndexChanged += new System.EventHandler(this.CmbEstado_SelectedIndexChanged);
            // 
            // cmbCiudad
            // 
            this.cmbCiudad.FormattingEnabled = true;
            this.cmbCiudad.Location = new System.Drawing.Point(556, 74);
            this.cmbCiudad.Name = "cmbCiudad";
            this.cmbCiudad.Size = new System.Drawing.Size(169, 21);
            this.cmbCiudad.TabIndex = 10;
            // 
            // txtClave
            // 
            this.txtClave.Location = new System.Drawing.Point(10, 113);
            this.txtClave.Name = "txtClave";
            this.txtClave.Size = new System.Drawing.Size(128, 20);
            this.txtClave.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Su nueva clave:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(499, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Fecha de ingreso";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(369, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Estado";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(553, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Ciudad";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(144, 99);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Titulo profesional";
            // 
            // txtTitulo
            // 
            this.txtTitulo.Location = new System.Drawing.Point(144, 113);
            this.txtTitulo.Name = "txtTitulo";
            this.txtTitulo.Size = new System.Drawing.Size(219, 20);
            this.txtTitulo.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(369, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Cedula";
            // 
            // txtCedula
            // 
            this.txtCedula.Location = new System.Drawing.Point(369, 113);
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(123, 20);
            this.txtCedula.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(496, 98);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Fecha de Nacimiento";
            // 
            // dtpNacimiento
            // 
            this.dtpNacimiento.Location = new System.Drawing.Point(499, 113);
            this.dtpNacimiento.Name = "dtpNacimiento";
            this.dtpNacimiento.Size = new System.Drawing.Size(226, 20);
            this.dtpNacimiento.TabIndex = 20;
            // 
            // lblClave
            // 
            this.lblClave.AutoSize = true;
            this.lblClave.Location = new System.Drawing.Point(748, 31);
            this.lblClave.Name = "lblClave";
            this.lblClave.Size = new System.Drawing.Size(13, 13);
            this.lblClave.TabIndex = 22;
            this.lblClave.Text = "0";
            // 
            // dtgMaestros
            // 
            this.dtgMaestros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgMaestros.Location = new System.Drawing.Point(14, 204);
            this.dtgMaestros.Name = "dtgMaestros";
            this.dtgMaestros.Size = new System.Drawing.Size(832, 222);
            this.dtgMaestros.TabIndex = 23;
            this.dtgMaestros.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgMaestros_CellClick);
            this.dtgMaestros.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgMaestros_CellDoubleClick);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(131, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Estudios";
            // 
            // cmbEstudios
            // 
            this.cmbEstudios.FormattingEnabled = true;
            this.cmbEstudios.Items.AddRange(new object[] {
            "Licenciatura",
            "Maestría",
            "Especialidad",
            "Postgrado",
            "Doctorado"});
            this.cmbEstudios.Location = new System.Drawing.Point(134, 37);
            this.cmbEstudios.Name = "cmbEstudios";
            this.cmbEstudios.Size = new System.Drawing.Size(133, 21);
            this.cmbEstudios.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(280, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Area";
            // 
            // txtArea
            // 
            this.txtArea.Location = new System.Drawing.Point(283, 38);
            this.txtArea.Name = "txtArea";
            this.txtArea.Size = new System.Drawing.Size(233, 20);
            this.txtArea.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(525, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "Periodo";
            // 
            // txtPeriodo
            // 
            this.txtPeriodo.Location = new System.Drawing.Point(528, 38);
            this.txtPeriodo.Name = "txtPeriodo";
            this.txtPeriodo.Size = new System.Drawing.Size(180, 20);
            this.txtPeriodo.TabIndex = 32;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.Location = new System.Drawing.Point(508, 432);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(75, 72);
            this.btnNuevo.TabIndex = 33;
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(589, 432);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 72);
            this.btnGuardar.TabIndex = 34;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(670, 432);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 72);
            this.btnCancelar.TabIndex = 35;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.Location = new System.Drawing.Point(751, 432);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 72);
            this.btnEliminar.TabIndex = 36;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // cmbInvisible
            // 
            this.cmbInvisible.FormattingEnabled = true;
            this.cmbInvisible.Location = new System.Drawing.Point(7, 432);
            this.cmbInvisible.Name = "cmbInvisible";
            this.cmbInvisible.Size = new System.Drawing.Size(121, 21);
            this.cmbInvisible.TabIndex = 37;
            // 
            // gpoEstudios
            // 
            this.gpoEstudios.Controls.Add(this.txtArchivo);
            this.gpoEstudios.Controls.Add(this.label18);
            this.gpoEstudios.Controls.Add(this.label16);
            this.gpoEstudios.Controls.Add(this.txtClaveMaestro);
            this.gpoEstudios.Controls.Add(this.label12);
            this.gpoEstudios.Controls.Add(this.btnEliminarDoc);
            this.gpoEstudios.Controls.Add(this.btnCargar);
            this.gpoEstudios.Controls.Add(this.txtPeriodo);
            this.gpoEstudios.Controls.Add(this.label15);
            this.gpoEstudios.Controls.Add(this.txtArea);
            this.gpoEstudios.Controls.Add(this.label14);
            this.gpoEstudios.Controls.Add(this.cmbEstudios);
            this.gpoEstudios.Controls.Add(this.label13);
            this.gpoEstudios.Location = new System.Drawing.Point(16, 19);
            this.gpoEstudios.Name = "gpoEstudios";
            this.gpoEstudios.Size = new System.Drawing.Size(757, 121);
            this.gpoEstudios.TabIndex = 38;
            this.gpoEstudios.TabStop = false;
            // 
            // txtArchivo
            // 
            this.txtArchivo.Location = new System.Drawing.Point(134, 83);
            this.txtArchivo.Name = "txtArchivo";
            this.txtArchivo.Size = new System.Drawing.Size(382, 20);
            this.txtArchivo.TabIndex = 39;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(134, 67);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 13);
            this.label18.TabIndex = 38;
            this.label18.Text = "Nombre del archivo";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "Clave de maestro";
            // 
            // txtClaveMaestro
            // 
            this.txtClaveMaestro.Location = new System.Drawing.Point(7, 37);
            this.txtClaveMaestro.Name = "txtClaveMaestro";
            this.txtClaveMaestro.Size = new System.Drawing.Size(111, 20);
            this.txtClaveMaestro.TabIndex = 36;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 13);
            this.label12.TabIndex = 35;
            this.label12.Text = "Cargar Archivos";
            // 
            // btnEliminarDoc
            // 
            this.btnEliminarDoc.Location = new System.Drawing.Point(55, 83);
            this.btnEliminarDoc.Name = "btnEliminarDoc";
            this.btnEliminarDoc.Size = new System.Drawing.Size(41, 20);
            this.btnEliminarDoc.TabIndex = 34;
            this.btnEliminarDoc.Text = "x";
            this.btnEliminarDoc.UseVisualStyleBackColor = true;
            this.btnEliminarDoc.Click += new System.EventHandler(this.BtnEliminarDoc_Click);
            // 
            // btnCargar
            // 
            this.btnCargar.Location = new System.Drawing.Point(8, 83);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(41, 20);
            this.btnCargar.TabIndex = 33;
            this.btnCargar.Text = "...";
            this.btnCargar.UseVisualStyleBackColor = true;
            this.btnCargar.Click += new System.EventHandler(this.BtnCargar_Click);
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(748, 59);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(13, 13);
            this.lblID.TabIndex = 39;
            this.lblID.Text = "0";
            // 
            // tbcMaestros
            // 
            this.tbcMaestros.Controls.Add(this.tabMaestros);
            this.tbcMaestros.Controls.Add(this.tabEstudios);
            this.tbcMaestros.Location = new System.Drawing.Point(1, 1);
            this.tbcMaestros.Name = "tbcMaestros";
            this.tbcMaestros.SelectedIndex = 0;
            this.tbcMaestros.Size = new System.Drawing.Size(866, 536);
            this.tbcMaestros.TabIndex = 40;
            // 
            // tabMaestros
            // 
            this.tabMaestros.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabMaestros.Controls.Add(this.rbtNo);
            this.tabMaestros.Controls.Add(this.rbtSi);
            this.tabMaestros.Controls.Add(this.label17);
            this.tabMaestros.Controls.Add(this.cmbInvisible);
            this.tabMaestros.Controls.Add(this.lblID);
            this.tabMaestros.Controls.Add(this.btnEliminar);
            this.tabMaestros.Controls.Add(this.btnCancelar);
            this.tabMaestros.Controls.Add(this.btnGuardar);
            this.tabMaestros.Controls.Add(this.btnNuevo);
            this.tabMaestros.Controls.Add(this.dtpNacimiento);
            this.tabMaestros.Controls.Add(this.txtClave);
            this.tabMaestros.Controls.Add(this.dtgMaestros);
            this.tabMaestros.Controls.Add(this.label5);
            this.tabMaestros.Controls.Add(this.txtTitulo);
            this.tabMaestros.Controls.Add(this.label9);
            this.tabMaestros.Controls.Add(this.lblClave);
            this.tabMaestros.Controls.Add(this.txtCedula);
            this.tabMaestros.Controls.Add(this.label8);
            this.tabMaestros.Controls.Add(this.label11);
            this.tabMaestros.Controls.Add(this.label7);
            this.tabMaestros.Controls.Add(this.label10);
            this.tabMaestros.Controls.Add(this.label6);
            this.tabMaestros.Controls.Add(this.cmbCiudad);
            this.tabMaestros.Controls.Add(this.label1);
            this.tabMaestros.Controls.Add(this.cmbEstado);
            this.tabMaestros.Controls.Add(this.txtNombre);
            this.tabMaestros.Controls.Add(this.label4);
            this.tabMaestros.Controls.Add(this.label2);
            this.tabMaestros.Controls.Add(this.txtDireccion);
            this.tabMaestros.Controls.Add(this.txtApp);
            this.tabMaestros.Controls.Add(this.dtpIngreso);
            this.tabMaestros.Controls.Add(this.label3);
            this.tabMaestros.Controls.Add(this.txtApm);
            this.tabMaestros.Location = new System.Drawing.Point(4, 22);
            this.tabMaestros.Name = "tabMaestros";
            this.tabMaestros.Padding = new System.Windows.Forms.Padding(3);
            this.tabMaestros.Size = new System.Drawing.Size(858, 510);
            this.tabMaestros.TabIndex = 0;
            this.tabMaestros.Text = "Maestros";
            this.tabMaestros.Click += new System.EventHandler(this.TabMaestros_Click);
            // 
            // rbtNo
            // 
            this.rbtNo.AutoSize = true;
            this.rbtNo.Location = new System.Drawing.Point(99, 169);
            this.rbtNo.Name = "rbtNo";
            this.rbtNo.Size = new System.Drawing.Size(39, 17);
            this.rbtNo.TabIndex = 42;
            this.rbtNo.TabStop = true;
            this.rbtNo.Text = "No";
            this.rbtNo.UseVisualStyleBackColor = true;
            // 
            // rbtSi
            // 
            this.rbtSi.AutoSize = true;
            this.rbtSi.Location = new System.Drawing.Point(13, 169);
            this.rbtSi.Name = "rbtSi";
            this.rbtSi.Size = new System.Drawing.Size(34, 17);
            this.rbtSi.TabIndex = 41;
            this.rbtSi.TabStop = true;
            this.rbtSi.Text = "Si";
            this.rbtSi.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 140);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(142, 13);
            this.label17.TabIndex = 40;
            this.label17.Text = "¿Cuenta con otros estudios?";
            // 
            // tabEstudios
            // 
            this.tabEstudios.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabEstudios.Controls.Add(this.lblEstudios);
            this.tabEstudios.Controls.Add(this.dtgEstudios);
            this.tabEstudios.Controls.Add(this.btnEliminarEstudio);
            this.tabEstudios.Controls.Add(this.btnCancelarEstudio);
            this.tabEstudios.Controls.Add(this.button1);
            this.tabEstudios.Controls.Add(this.gpoEstudios);
            this.tabEstudios.Location = new System.Drawing.Point(4, 22);
            this.tabEstudios.Name = "tabEstudios";
            this.tabEstudios.Padding = new System.Windows.Forms.Padding(3);
            this.tabEstudios.Size = new System.Drawing.Size(858, 510);
            this.tabEstudios.TabIndex = 1;
            this.tabEstudios.Text = "Estudios";
            this.tabEstudios.Click += new System.EventHandler(this.TabEstudios_Click);
            // 
            // lblEstudios
            // 
            this.lblEstudios.AutoSize = true;
            this.lblEstudios.Location = new System.Drawing.Point(789, 28);
            this.lblEstudios.Name = "lblEstudios";
            this.lblEstudios.Size = new System.Drawing.Size(13, 13);
            this.lblEstudios.TabIndex = 43;
            this.lblEstudios.Text = "0";
            // 
            // dtgEstudios
            // 
            this.dtgEstudios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgEstudios.Location = new System.Drawing.Point(16, 162);
            this.dtgEstudios.Name = "dtgEstudios";
            this.dtgEstudios.Size = new System.Drawing.Size(816, 216);
            this.dtgEstudios.TabIndex = 42;
            this.dtgEstudios.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgEstudios_CellDoubleClick);
            // 
            // btnEliminarEstudio
            // 
            this.btnEliminarEstudio.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarEstudio.Image")));
            this.btnEliminarEstudio.Location = new System.Drawing.Point(757, 435);
            this.btnEliminarEstudio.Name = "btnEliminarEstudio";
            this.btnEliminarEstudio.Size = new System.Drawing.Size(75, 69);
            this.btnEliminarEstudio.TabIndex = 41;
            this.btnEliminarEstudio.UseVisualStyleBackColor = true;
            this.btnEliminarEstudio.Click += new System.EventHandler(this.BtnEliminarEstudio_Click);
            // 
            // btnCancelarEstudio
            // 
            this.btnCancelarEstudio.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelarEstudio.Image")));
            this.btnCancelarEstudio.Location = new System.Drawing.Point(676, 435);
            this.btnCancelarEstudio.Name = "btnCancelarEstudio";
            this.btnCancelarEstudio.Size = new System.Drawing.Size(75, 69);
            this.btnCancelarEstudio.TabIndex = 40;
            this.btnCancelarEstudio.UseVisualStyleBackColor = true;
            this.btnCancelarEstudio.Click += new System.EventHandler(this.BtnCancelarEstudio_Click);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(595, 435);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 69);
            this.button1.TabIndex = 39;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Maestros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 580);
            this.Controls.Add(this.tbcMaestros);
            this.Name = "Maestros";
            this.Text = "Maestros";
            this.Load += new System.EventHandler(this.Maestros_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgMaestros)).EndInit();
            this.gpoEstudios.ResumeLayout(false);
            this.gpoEstudios.PerformLayout();
            this.tbcMaestros.ResumeLayout(false);
            this.tabMaestros.ResumeLayout(false);
            this.tabMaestros.PerformLayout();
            this.tabEstudios.ResumeLayout(false);
            this.tabEstudios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEstudios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtApp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtApm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpIngreso;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbEstado;
        private System.Windows.Forms.ComboBox cmbCiudad;
        private System.Windows.Forms.TextBox txtClave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTitulo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCedula;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpNacimiento;
        private System.Windows.Forms.Label lblClave;
        private System.Windows.Forms.DataGridView dtgMaestros;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbEstudios;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtArea;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPeriodo;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.ComboBox cmbInvisible;
        private System.Windows.Forms.GroupBox gpoEstudios;
        private System.Windows.Forms.Button btnEliminarDoc;
        private System.Windows.Forms.Button btnCargar;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TabControl tbcMaestros;
        private System.Windows.Forms.TabPage tabMaestros;
        private System.Windows.Forms.TabPage tabEstudios;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dtgEstudios;
        private System.Windows.Forms.Button btnEliminarEstudio;
        private System.Windows.Forms.Button btnCancelarEstudio;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtClaveMaestro;
        private System.Windows.Forms.Label lblEstudios;
        private System.Windows.Forms.RadioButton rbtNo;
        private System.Windows.Forms.RadioButton rbtSi;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtArchivo;
        private System.Windows.Forms.Label label18;
    }
}