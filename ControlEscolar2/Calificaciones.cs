﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocios.ControlEscolar2;
using Entidades.ControlEscolar2;
using Exportar_PDF_Excel;
using System.IO;

namespace ControlEscolar2
{
    public partial class Calificaciones : Form
    {
        CalificacionesManejador _calificacionesManejador;
        Exportar_Archivos exportar_Archivos;
        int FkAlumnoGlobal = 0;
        int fkGrupoGlobal = 0;
        int fkMateriaGlobal = 0;
        int Editar_asignacion = 0;
        int IDCalificacion = 0;
        string _ruta;
        bool ComprobarExcel = true;
      
        public Calificaciones()
        {
            InitializeComponent();
            _calificacionesManejador = new CalificacionesManejador();
            exportar_Archivos = new Exportar_Archivos();
            _ruta = Application.StartupPath + "\\DocumentosPDF\\";
        }

        private void Calificaciones_Load(object sender, EventArgs e)
        {
            cmbGrupo.DataSource = _calificacionesManejador.LLenarComboGrupos().Tables[0];
            cmbGrupo.DisplayMember = "claveGrupo";
            
        }

        private void CmbGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbMateria.DataSource = _calificacionesManejador.CargarCombos(cmbGrupo.Text).Tables[0];
            cmbMateria.DisplayMember = "nombreMateria";
            cmbAlumnos.DataSource= _calificacionesManejador.CargarCombos(cmbGrupo.Text).Tables[1];
            cmbAlumnos.DisplayMember = "NumControl";

            if (_calificacionesManejador.IDGrupo(cmbGrupo.Text).Tables[0].Rows.Count == 0)
            {  }
            else
            {
                string fkGrupo = _calificacionesManejador.IDGrupo(cmbGrupo.Text).Tables[0].Rows[0]["IDGrupo"].ToString();
                fkGrupoGlobal = int.Parse(fkGrupo);
                
            }
        }

        private void CmbAlumnos_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNombreAlumno.Text = _calificacionesManejador.IDAlumno(int.Parse(cmbAlumnos.Text)).Tables[0].Rows[0]["nombre"].ToString();
            txtApp.Text = _calificacionesManejador.IDAlumno(int.Parse(cmbAlumnos.Text)).Tables[0].Rows[0]["app"].ToString();
            txtApm.Text = _calificacionesManejador.IDAlumno(int.Parse(cmbAlumnos.Text)).Tables[0].Rows[0]["apm"].ToString();
            string fkAlumno = _calificacionesManejador.IDAlumno(int.Parse(cmbAlumnos.Text)).Tables[0].Rows[0]["ID"].ToString();
            FkAlumnoGlobal = int.Parse(fkAlumno);
            
            MostrarCalificaciones(FkAlumnoGlobal);
        }

        private void CmbMateria_SelectedIndexChanged(object sender, EventArgs e)
        {
            string fkMateria = _calificacionesManejador.IDMateria(cmbMateria.Text).Tables[0].Rows[0]["IDMateriaAuto"].ToString();
            fkMateriaGlobal = int.Parse(fkMateria);
           
        }
        void MostrarCalificaciones(int filtro)
        {
         
                if (txtNombreAlumno.Text == "")
                { }
                else
                {
                    dtgCalificaciones.DataSource = _calificacionesManejador.LLenarGridCalificaciones(filtro).Tables[0];
                }
          
           
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ComprobarMateriaRepetida() == 0)
                {


                    if (ComprobarTextBox() == 0)
                    {
                        _calificacionesManejador.Guardar(new Entidades.ControlEscolar2.Calificaciones
                        {
                            FkAlumno = FkAlumnoGlobal,
                            FkGrupo = fkGrupoGlobal,
                            FkMateria = fkMateriaGlobal,
                            Parcial_1 = int.Parse(txtParcial1.Text),
                            Parcial_2 = int.Parse(txtParcial2.Text),
                            Parcial_3 = int.Parse(txtParcial3.Text),
                            Parcial_4 = int.Parse(txtParcial4.Text)

                        }, Editar_asignacion);
                        MostrarCalificaciones(FkAlumnoGlobal);
                        Limpiar();
                    }
                    else
                        MessageBox.Show("Debe llenar todas las calificaciones");

                }
                else
                {
                    if (Editar_asignacion != 0)
                    {

                        if (ComprobarTextBox() == 0)
                        {
                            _calificacionesManejador.Guardar(new Entidades.ControlEscolar2.Calificaciones
                            {

                                Parcial_1 = int.Parse(txtParcial1.Text),
                                Parcial_2 = int.Parse(txtParcial2.Text),
                                Parcial_3 = int.Parse(txtParcial3.Text),
                                Parcial_4 = int.Parse(txtParcial4.Text)

                            }, Editar_asignacion);
                            MostrarCalificaciones(FkAlumnoGlobal);
                            Limpiar();
                        }
                        else
                            MessageBox.Show("Debe llenar todas las calificaciones");

                    }
                    else
                        MessageBox.Show("Esa materia ya está cargada para ese alumno");
                }
            }
            catch { }
                
        }
        private int ComprobarTextBox()
        {
            
           if(txtParcial1.Text=="" || txtParcial2.Text == ""|| txtParcial3.Text == ""|| txtParcial4.Text == "")
                return 1;
           else
            return 0;
        }
        private int ComprobarMateriaRepetida()
        {
            DataSet ds = _calificacionesManejador.ComprobarMateriaenAlumno(FkAlumnoGlobal, fkMateriaGlobal);
            if (ds.Tables[0].Rows.Count == 0)
                return 0;
            else
                return 1;
           
        }
        void Limpiar()
        {
            txtApm.Clear();
            txtApp.Clear();
            txtNombreAlumno.Clear();
            fkGrupoGlobal = 0;
            fkMateriaGlobal = 0;
            FkAlumnoGlobal = 0;
            Editar_asignacion = 0;
            txtParcial1.Clear();
            txtParcial2.Clear();
            txtParcial3.Clear();
            txtParcial4.Clear();
            IDCalificacion = 0;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void DtgCalificaciones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string IDCalificacion = dtgCalificaciones.CurrentRow.Cells["IDCalificacion"].Value.ToString();
                Editar_asignacion = int.Parse(IDCalificacion);
                txtNombreAlumno.Text = _calificacionesManejador.IDAlumno(int.Parse(cmbAlumnos.Text)).Tables[0].Rows[0]["nombre"].ToString();
                txtApp.Text = _calificacionesManejador.IDAlumno(int.Parse(cmbAlumnos.Text)).Tables[0].Rows[0]["app"].ToString();
                txtApm.Text = _calificacionesManejador.IDAlumno(int.Parse(cmbAlumnos.Text)).Tables[0].Rows[0]["apm"].ToString();
                txtParcial1.Text = dtgCalificaciones.CurrentRow.Cells["Parcial 1"].Value.ToString();
                txtParcial2.Text = dtgCalificaciones.CurrentRow.Cells["Parcial 2"].Value.ToString();
                txtParcial3.Text = dtgCalificaciones.CurrentRow.Cells["Parcial 3"].Value.ToString();
                txtParcial4.Text = dtgCalificaciones.CurrentRow.Cells["Parcial 4"].Value.ToString();

               
                string fkAlumno = _calificacionesManejador.IDAlumno(int.Parse(cmbAlumnos.Text)).Tables[0].Rows[0]["ID"].ToString();
                FkAlumnoGlobal = int.Parse(fkAlumno);
                
            }
            catch { }

        }

        private void BtnExportarPDF_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (Directory.Exists(_ruta))
                {
                    exportar_Archivos.ExportarTabla_PDF(_calificacionesManejador.LLenarGridCalificaciones(FkAlumnoGlobal).Tables[0], _ruta + txtNombreAlumno.Text + "_" + txtApp.Text + "_" + txtApm.Text + ".pdf", "Boleta", txtNombreAlumno.Text + " " + txtApp.Text + " " + txtApm.Text);
                    MessageBox.Show("Archivo creado correctamente en: " + _ruta);
                }
                else
                {
                    Directory.CreateDirectory(_ruta);
                    exportar_Archivos.ExportarTabla_PDF(_calificacionesManejador.LLenarGridCalificaciones(FkAlumnoGlobal).Tables[0], _ruta + txtNombreAlumno.Text + "_" + txtApp.Text + "_" + txtApm.Text + ".pdf", "Boleta", txtNombreAlumno.Text + " " + txtApp.Text + " " + txtApm.Text);
                    MessageBox.Show("Archivo creado correctamente en: " + _ruta);
                }
            }
            catch(Exception ex) { MessageBox.Show(ex.Message); }
        }
        public bool ExportarExcel()
        {
            try
            {

                MostrarCalificaciones(FkAlumnoGlobal);
                exportar_Archivos.ExportarDataGridViewExcel(dtgCalificaciones);
               // MessageBox.Show("Archivo creado");
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        private void BtnExportarExcel_Click(object sender, EventArgs e)
        {
           
             try
             {

                 MostrarCalificaciones(FkAlumnoGlobal);
                 exportar_Archivos.ExportarDataGridViewExcel(dtgCalificaciones);
                 MessageBox.Show("Archivo creado");

             }
             catch
             {

             }
             
        }

        private void DtgCalificaciones_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                string IDCalificaciones = dtgCalificaciones.CurrentRow.Cells["IDCalificacion"].Value.ToString();
                IDCalificacion = int.Parse(IDCalificaciones);
            }
            catch { }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro de eliminar este registro?","Eliminar calificacion",MessageBoxButtons.YesNo,MessageBoxIcon.Warning)==DialogResult.Yes)
            {
                try
                {
                    _calificacionesManejador.Eliminar(IDCalificacion);
                    
                }
                catch { }
                MostrarCalificaciones(FkAlumnoGlobal);
                Limpiar();
            }
        }

        private void BgwExportar_DoWork(object sender, DoWorkEventArgs e)
        {

            dtgCalificaciones.DataSource = _calificacionesManejador.LLenarGridCalificaciones(FkAlumnoGlobal).Tables[0];
            exportar_Archivos.ExportarDataGridViewExcel(dtgCalificaciones);

        }

        private void BgwExportar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
                if (e.Error != null)
                {

                }
                else
                {
                    MessageBox.Show("Archivo creado correctamente");
                }
            
          
        }
    }
}
