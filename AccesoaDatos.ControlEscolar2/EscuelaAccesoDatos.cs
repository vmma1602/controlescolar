﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
namespace AccesoaDatos.ControlEscolar2
    
{
   public class EscuelaAccesoDatos
    {
        ConexionAccesoaDatos conexion;
        public EscuelaAccesoDatos()
        {
            conexion = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Entidades.ControlEscolar2.Escuela escuela, int x)
        {
            if(x==0)
            {
                //insertar'
                string consulta = "insert into escuela values(null,'" + escuela.NombreEscuela + "','" + escuela.Rfc + "','" + escuela.Domicilio + "','" + escuela.NumTel + "','" + escuela.Email + "','" + escuela.PagWeb + "','" + escuela.LogoNombre + "','" + escuela.Director + "')";
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = "update escuela set nombreEscuela='" + escuela.NombreEscuela + "',rfc='" + escuela.Rfc + "',domicilio ='" + escuela.Domicilio + "',telefono='" + escuela.NumTel + "',email='" + escuela.Email + "',PagWeb='" + escuela.PagWeb + "',logoNombre='" + escuela.LogoNombre + "',nombredir='" + escuela.Director + "' where IDEscuela= "+x+"";
                conexion.EjecutarConsulta(consulta);
            }


        }
        public void Eliminar()
        {
            string consulta = "call ArmarTabla()";
            conexion.EjecutarConsulta(consulta);

        }
        public List<Entidades.ControlEscolar2.Escuela> GetEscuela(string filtro)
        {
            var ListEscuela = new List<Entidades.ControlEscolar2.Escuela>();
            var ds = new DataSet();
            string consulta = "select * from escuela";
            ds = conexion.ObtenerDatos(consulta, "escuela");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var escuela = new Entidades.ControlEscolar2.Escuela
                {
                   IDEscuela=Convert.ToInt32(row["IDEscuela"]),
                   NombreEscuela=row["nombreEscuela"].ToString(),
                   Rfc= row["rfc"].ToString(),
                   Domicilio= row["domicilio"].ToString(),
                   Director= row["nombreDir"].ToString(),
                   NumTel= row["telefono"].ToString(),
                   Email= row["email"].ToString(),
                   PagWeb= row["PagWeb"].ToString(),
                   LogoNombre= row["logoNombre"].ToString(),
                   

            };
                ListEscuela.Add(escuela);
            }
            //Llenar lista
            return ListEscuela;
        }
        public DataSet IDUnico(string filtro)
        {
            DataSet ds;
            string consulta = "SELECT * from escuela";
            ds = conexion.ObtenerDatos(consulta, "escuela");
            return ds;
        }
    }
}
