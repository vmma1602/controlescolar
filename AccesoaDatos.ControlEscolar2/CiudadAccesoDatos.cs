﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;


namespace AccesoaDatos.ControlEscolar2
{
    
    public class CiudadAccesoDatos
    {
        ConexionAccesoaDatos conexion;
        public CiudadAccesoDatos()
        {
            conexion = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public List<Municipios> GetciudadesList(string estado)
        {
            var listMunicipios = new List<Municipios>();
            var ds = new DataSet();
            string consulta = "select NombreMun from municipios where fkEstado ='"+estado+"'";
            ds = conexion.ObtenerDatos(consulta, "municipios");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var municipios = new Municipios
                {
                   NombreMun=row["nombreMun"].ToString(),

                };
                listMunicipios.Add(municipios);
            }
            return listMunicipios;
        }
        
    }
}
