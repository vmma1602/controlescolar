﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;

namespace AccesoaDatos.ControlEscolar2
{
    public class AlumnoAccesoDatos
    {
        
        ConexionAccesoaDatos conexion;
        public AlumnoAccesoDatos()
        {
            conexion = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Alumnos alumno)
        {

            if (alumno.ID == 0)
            {
                //insertar
                string consulta = "insert into alumno values(null," + alumno.NumControl + ",'" + alumno.Nombre + "','" + alumno.App + "','" + alumno.Apm + "','" + alumno.Domicilio + "','" + alumno.Email + "','" + alumno.Fecha + "','" + alumno.Sexo + "','" + alumno.Estado + "','" + alumno.Ciudad + "')";
                conexion.EjecutarConsulta(consulta);


            }
            else
            {
                //update
                string consulta = "update  alumno set  nombre='" + alumno.Nombre + "',app='" + alumno.App + "',apm='" + alumno.Apm + "',domicilio='" + alumno.Domicilio + "',email='" + alumno.Email + "',sexo='" + alumno.Sexo + "', fecha='" + alumno.Fecha + "', estado='" + alumno.Estado + "', ciudad='" + alumno.Ciudad + "' where ID=" + alumno.ID + "";
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int ID)
        {
            //eliminar
            string consulta = "delete from alumno where ID=" + ID + "";
            conexion.EjecutarConsulta(consulta);

        }
        public List<Alumnos> GetAlumnos(string filtro)
        {
            var listAlumnos = new List<Alumnos>();
            var ds = new DataSet();
            string consulta = "select * from alumno where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "alumno");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var alumno = new Alumnos
                {
                    ID = Convert.ToInt32(row["ID"]),
                    NumControl = Convert.ToInt32(row["NumControl"]),
                    Nombre = row["nombre"].ToString(),
                    Apm = row["apm"].ToString(),
                    App = row["app"].ToString(),
                    Domicilio = row["domicilio"].ToString(),
                    Email = row["email"].ToString(),
                    Fecha = row["fecha"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Estado = row["estado"].ToString(),
                    Ciudad = row["ciudad"].ToString()

                };
                listAlumnos.Add(alumno);
            }
            //Llenar lista
            return listAlumnos;
        }
      
       
        
    }
}
