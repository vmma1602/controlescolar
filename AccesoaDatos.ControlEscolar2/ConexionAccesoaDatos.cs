﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;

namespace AccesoaDatos.ControlEscolar2
{
    public class ConexionAccesoaDatos
    {
        private MySqlConnection conn;
        public ConexionAccesoaDatos(string servidor, string usuario, string password, string database, uint puerto)
        {
            MySqlConnectionStringBuilder cadenaConexion = new MySqlConnectionStringBuilder();
            cadenaConexion.Server = servidor;
            cadenaConexion.UserID = usuario;
            cadenaConexion.Password = password;
            cadenaConexion.Database = database;
            cadenaConexion.Port = puerto;         
            conn = new MySqlConnection(cadenaConexion.ToString());
        }
        public void EjecutarConsulta(string consulta)
        {

            conn.Open();
            var command = new MySqlCommand(consulta, conn);
            command.ExecuteNonQuery();
            conn.Close();

        }
        public DataSet ObtenerDatos(string consulta, string tabla)
        {
            var ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(consulta, conn);
            da.Fill(ds, tabla);
            return ds;
        }
      
    }
}
