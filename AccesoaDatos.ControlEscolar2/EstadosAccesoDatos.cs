﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;


namespace AccesoaDatos.ControlEscolar2
{
    public class EstadosAccesoDatos
    {
        ConexionAccesoaDatos conexion;
        public EstadosAccesoDatos()
        {
            conexion = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public List<Estados> GetEstadosList()
        {
            var listEstados = new List<Estados>();
            var ds = new DataSet();
            string consulta = "select nombre, CodigoEstado from estados";
            ds = conexion.ObtenerDatos(consulta, "estados");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var estados = new Estados
                {                    

                    Nombre=row["nombre"].ToString(),                                       
                };
                listEstados.Add(estados);
            }            
            return listEstados;
        }
        public List<Estados> CodigoEstado(string x)
        {
            var listEstados = new List<Estados>();
            var ds = new DataSet();
            string consulta = "select CodigoEstado from estados where estados.nombre='"+x+"'";
            ds = conexion.ObtenerDatos(consulta, "estados");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var estados = new Estados
                {
                  CodigoEstado=row["CodigoEstado"].ToString()
                };
                listEstados.Add(estados);
            }
            return listEstados;
        }

    }
}
