﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
namespace AccesoaDatos.ControlEscolar2
{
    public class GrupoAccesoDatos
    {
        ConexionAccesoaDatos conexion;
        public GrupoAccesoDatos()
        {
            conexion = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Entidades.ControlEscolar2.Grupo grupo, int x)
        {
            if (x == 0)
            {
                //insertar'
                string consulta = "insert into grupo values(null,'" +grupo.NombreGrupo+ "','" + grupo.Turno + "','" + grupo.Carrera + "'," + grupo.Semestre + ")";
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = "update grupo set nombreGrupo='" + grupo.NombreGrupo + "',turno='" + grupo.Turno + "',semestre =" + grupo.Semestre + ", carrera ='"+grupo.Carrera+"' where IDGrupo ="+x+"";
                conexion.EjecutarConsulta(consulta);
            }


        }
        public void Eliminar(int IDGrupo)
        {
           
            string consulta1 = "delete from asignacion_alumnos where fkGrupoID =" + IDGrupo + "";
            conexion.EjecutarConsulta(consulta1);
            string consulta2 = "delete from grupo where IDGrupo ="+IDGrupo+"";
            conexion.EjecutarConsulta(consulta2);

        }
        public List<Entidades.ControlEscolar2.Grupo> GetGrupo(string filtro)
        {
            var ListGrupo = new List<Entidades.ControlEscolar2.Grupo>();
            var ds = new DataSet();
            string consulta = "select * from grupo";
            ds = conexion.ObtenerDatos(consulta, "grupo");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var grupo = new Entidades.ControlEscolar2.Grupo
                {
                    IDGrupo=Convert.ToInt32(row["IDGrupo"]),
                    NombreGrupo=row["nombreGrupo"].ToString(),
                    Turno= row["turno"].ToString(),
                     Semestre=Convert.ToInt32(row["semestre"]),
                     Carrera=row["carrera"].ToString()
                      

                };
                ListGrupo.Add(grupo);
            }
            //Llenar lista
            return ListGrupo;
        }
        public DataSet IDGrupoDS()
        {
            DataSet ds;
            string consulta = "SELECT * from grupo";
            ds = conexion.ObtenerDatos(consulta, "grupo");
            return ds;
        }
      
    }
}

