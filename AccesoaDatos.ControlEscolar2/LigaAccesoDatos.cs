﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace AccesoaDatos.ControlEscolar2
{
    public class LigaAccesoDatos
    {
        ConexionAccesoaDatos conn;
        public LigaAccesoDatos()
        {
            conn = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Entidades.ControlEscolar2.Liga liga)
        {            
                //insertar'
                string consulta = "insert into liga values(null,'" + liga.IDMateria + "','" + liga.Primera + "','" + liga.Tercera + "')";
                conn.EjecutarConsulta(consulta);

            
        }
        public void Eliminar(int IDMateria)
        {
            //eliminar
           
            string consulta = "delete from liga where IDLiga = " + IDMateria + "";
            conn.EjecutarConsulta(consulta);

        }
        public List<Entidades.ControlEscolar2.Liga> GetLiga(string filtro)
        {
            var listLiga = new List<Entidades.ControlEscolar2.Liga>();
            var ds = new DataSet();
            string consulta = "select * from liga";
            ds = conn.ObtenerDatos(consulta, "liga");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var liga = new Entidades.ControlEscolar2.Liga
                {
                   IDMateria=row["IDMateriaActual"].ToString(),
                   Primera=row["fkAnterior"].ToString(),
                   Tercera=row["fkDespues"].ToString()


                };
                listLiga.Add(liga);
            }
            //Llenar lista
            return listLiga;
        }


      
        public DataSet dsLiga(string filtro)
        {
            DataSet ds;
            string consulta = "SELECT IDLiga FROM liga WHERE fkAnterior like '%" + filtro + "%' or fkDespues like '%" + filtro + "%' or IDMateriaActual like '%" + filtro + "%'";
                 ds = conn.ObtenerDatos(consulta, "liga");
            return ds;
        }
        public DataSet ObtenerTodo()
        {
            DataSet ds;
            string consulta = "SELECT * FROM liga";
            ds = conn.ObtenerDatos(consulta, "liga");
            return ds;
        }


        //Actualizar

        public void ActualizarPrimero(Entidades.ControlEscolar2.Liga liga, int filtro)
        {
            //insertar
            string consulta = "update liga set fkAnterior='"+liga.Primera+"' where IDLiga = "+filtro+ "";
                  conn.EjecutarConsulta(consulta);
        }
        public void ActualizarSegunda(Entidades.ControlEscolar2.Liga liga, int filtro)
        {
            //insertar'
            string consulta = "update liga set IDMateriaActual='" + liga.IDMateria + "' where IDLiga = " + filtro + "";
            conn.EjecutarConsulta(consulta);
        }
        public void ActualizarTercera(Entidades.ControlEscolar2.Liga liga, int filtro)
        {
            //insertar'
            string consulta = "update liga set fkDespues='" + liga.Tercera + "' where IDLiga = " + filtro + "";
            conn.EjecutarConsulta(consulta);
        }
      


    }
}
