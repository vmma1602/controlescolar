﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
namespace AccesoaDatos.ControlEscolar2
{
    public class MateriasAccesoDatos
    {
        ConexionAccesoaDatos conn;
        public MateriasAccesoDatos()
        {
             conn = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Entidades.ControlEscolar2.Materias materia, string x, int a)
        { 
        if (x=="0" && a==0)
              {
                //insertar
                string consulta = "insert into materias values(null,'" + materia.IDMateria + "','" + materia.NombreMateria + "'," + materia.HorasPractica + "," + materia.HorasTeoria + ")";
                 conn.EjecutarConsulta(consulta);
              }
            else
            {
                //update
                string consulta = "update  materias set  nombreMateria='" + materia.NombreMateria + "',horasTeoria=" + materia.HorasTeoria + ",horasPractica =" + materia.HorasPractica + " where IDMateriaAuto =" + a + "";
                conn.EjecutarConsulta(consulta);
              }
           }
        public void Eliminar(string IDMateria)
        {
            //eliminar
            
            string an = "delete from materias where IDMateria = '" + IDMateria + "'";
            conn.EjecutarConsulta(an);
            

        }
        public List<Entidades.ControlEscolar2.Materias> GetMaterias(string filtro)
        {
            var listMaterias = new List<Entidades.ControlEscolar2.Materias>();
            var ds = new DataSet();
            string consulta = "select * from materias";
            ds = conn.ObtenerDatos(consulta, "materias");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var materia = new Entidades.ControlEscolar2.Materias
                {
                   IDMateria=row["IDMateria"].ToString(),
                   NombreMateria=row["nombreMateria"].ToString(),
                   HorasPractica=Convert.ToInt32(row["horasPractica"]),
                   HorasTeoria = Convert.ToInt32(row["horasTeoria"])

                };
                listMaterias.Add(materia);
            }
            //Llenar lista
            return listMaterias;
        }
        public DataSet MaxID(string filtro)
        {
            DataSet ds;
            string consulta = "select IDMateria from materias where IDMateria like '%" + filtro + "%' ORDER BY IDMateriaAuto DESC";            
            ds = conn.ObtenerDatos(consulta, "materias");
            return ds;
        }
        public DataSet IDAutoincrementable(string filtro)
        {
            DataSet ds;
            string consulta = "select IDMateriaAuto from materias where IDMateria ='"+filtro+"'";
            ds = conn.ObtenerDatos(consulta, "materias");
            return ds;
        }

    }
}
