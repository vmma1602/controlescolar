﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
   public class Asignacion_AlumnosAccesoDatos
    {
        ConexionAccesoaDatos conexion;
        public Asignacion_AlumnosAccesoDatos()
        {
            conexion = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Entidades.ControlEscolar2.Asignacion_Alumnos asignacion, int x)
        {
            if (x == 0)
            {
                //insertar'
                string consulta = "insert into Asignacion_Alumnos values(null," + asignacion.NumControl + ",'" + asignacion.NombreGrupo + "'," + asignacion.FkAlumno + "," + asignacion.FkGrupo + ")";
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = "update Asignacion_Alumnos set nombreGrupo='" + asignacion.NombreGrupo + "',NumControl=" + asignacion.NumControl + ",fkAlumnoID="+asignacion.FkAlumno + ", fkGrupoID =" + asignacion.FkGrupo + " where AsignacionID =" + x + "";
                conexion.EjecutarConsulta(consulta);
            }


        }
        public void Eliminar(int IDAsignacion)
        {
            
            string consulta = "delete from Asignacion_Alumnos where AsignacionID = " + IDAsignacion + " ";
            conexion.EjecutarConsulta(consulta);

        }
        public List<Entidades.ControlEscolar2.Asignacion_Alumnos> GetAsignacion_Alumnos(string filtro)
        {
            var ListAsignacion_Alumnos = new List<Entidades.ControlEscolar2.Asignacion_Alumnos>();
            var ds = new DataSet();
            string consulta = "select * from Asignacion_Alumnos where nombreGrupo like '"+filtro+"'";
            ds = conexion.ObtenerDatos(consulta, "Asignacion_Alumnos");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var Asignacion_Alumnos = new Entidades.ControlEscolar2.Asignacion_Alumnos
                {
                   AsignacionID= Convert.ToInt32(row["AsignacionID"]),
                   NombreGrupo=row["nombreGrupo"].ToString(),
                   NumControl= Convert.ToInt32(row["NumControl"]),
                   FkAlumno = Convert.ToInt32(row["fkAlumnoID"]),
                   FkGrupo= Convert.ToInt32(row["fkGrupoID"])

                };
                ListAsignacion_Alumnos.Add(Asignacion_Alumnos);
            }
            //Llenar lista
            return ListAsignacion_Alumnos;
        }
        public DataSet TodoAlumnos(int x)
        {
            DataSet ds;
            string consulta = "SELECT * from Alumno,grupo,Asignacion_Alumnos where ID ="+x+"";  
            ds = conexion.ObtenerDatos(consulta, "Alumno");
            return ds;
        }
        public DataSet nombreGrupoDS(string nombreGrupo)
        {
            DataSet ds;
            string consulta = "select * from grupo where nombreGrupo = '"+ nombreGrupo + "'";  
            ds = conexion.ObtenerDatos(consulta, "grupo");
            return ds;
        }
        public DataSet ComprobarAlummnoenGrupo(int NumControl)
        {
            DataSet ds;
            string consulta = "select * from Asignacion_Alumnos where NumControl = " + NumControl + "";
            ds = conexion.ObtenerDatos(consulta, "Asignacion_Alumnos");
            return ds;
        }
    }
}
