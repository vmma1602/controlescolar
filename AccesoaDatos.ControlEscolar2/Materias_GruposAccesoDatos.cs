﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;


namespace AccesoaDatos.ControlEscolar2
{
    public class Materias_GruposAccesoDatos
    {
        ConexionAccesoaDatos conexion;
        public Materias_GruposAccesoDatos()
        {
            conexion = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Entidades.ControlEscolar2.Grupos_Materias grupo_materia, int x)
        {
            if (x == 0)
            {
                //insertar'
                string consulta = "insert into Materias_Grupos values(null," + grupo_materia.FkMateria + "," + grupo_materia.FkGrupo + ")";
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = "update Materias_Grupos set fkMateria=" + grupo_materia.FkMateria + ",fkGrupo=" + grupo_materia.FkGrupo + " where IDMateria_Grupo="+x+"";
                conexion.EjecutarConsulta(consulta);
            }


        }
        public void Eliminar(int IDMateria_Grupo)
        {

            string consulta1 = "delete from Materias_Grupos where IDMateria_Grupo =" + IDMateria_Grupo + "";
            conexion.EjecutarConsulta(consulta1);
            

        }
        public List<Entidades.ControlEscolar2.Grupos_Materias> GetGrupos_Materias()
        {
            var ListGrupo = new List<Entidades.ControlEscolar2.Grupos_Materias>();
            var ds = new DataSet();
            string consulta = "select * from Materias_Grupos";
            ds = conexion.ObtenerDatos(consulta, "Materias_Grupos");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var grupo = new Entidades.ControlEscolar2.Grupos_Materias
                {
                  IDMaterias_Grupos=Convert.ToInt32(row["IDMateria_Grupo"]),
                  FkGrupo= Convert.ToInt32(row["fkGrupo"]),
                  FkMateria= Convert.ToInt32(row["fkMateria"])

                };
                ListGrupo.Add(grupo);
            }
            //Llenar lista
            return ListGrupo;
        }
        public DataSet Grupos_MateriasDS()
        {
            DataSet ds;
            string consulta = "SELECT * from Materias_Grupos";
            ds = conexion.ObtenerDatos(consulta, "Materias_Grupos");
            return ds;
        }
    }
}
