﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace AccesoaDatos.ControlEscolar2
{
    public class Maestros_GruposAccesoDatos
    {
        ConexionAccesoaDatos conn;
        public Maestros_GruposAccesoDatos()
        {
            conn = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Entidades.ControlEscolar2.Maestros_Grupos maestro_grupo, int x)
        {

            if (x == 0)
            {
                //insertar
                string consulta = "insert into Maestros_Grupos values(null,'" + maestro_grupo.ClaveMaestro + "','" + maestro_grupo.ClaveGrupo + "','" + maestro_grupo.ClaveMateria + "')";
                conn.EjecutarConsulta(consulta);


            }
            else
            {
                //update
                string consulta = "update Maestros_Grupos set claveGrupo='" + maestro_grupo.ClaveGrupo + "',claveMaestro='" + maestro_grupo.ClaveMaestro + "',claveMateria='" + maestro_grupo.ClaveMateria + "' where IDMAestro_Grupo = "+x+"";
                conn.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int IDMAestro_Grupo)
        {
           
            string consulta = "delete from Maestros_Grupos where IDMAestro_Grupo=" + IDMAestro_Grupo + "";
            conn.EjecutarConsulta(consulta);

        }

        public List<Entidades.ControlEscolar2.Maestros_Grupos> GetMaestros_Grupos(string filtro)
        {
            var listMaestros_Grupos = new List<Entidades.ControlEscolar2.Maestros_Grupos>();
            var ds = new DataSet();
            string consulta = "select * from Maestros_Grupos";
            ds = conn.ObtenerDatos(consulta, "Maestros_Grupos");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var maestro_grupo = new Entidades.ControlEscolar2.Maestros_Grupos
                { 
                    IDMaestros_Grupos=Convert.ToInt32(row["IdMaestro_Grupo"]),
                   ClaveMateria=row["claveMateria"].ToString(),
                   ClaveMaestro=row["claveMaestro"].ToString(),
                   ClaveGrupo=row["claveGrupo"].ToString()
                };
                listMaestros_Grupos.Add(maestro_grupo);
            }
            //Llenar lista
            return listMaestros_Grupos;
        }
        public DataSet IDMaestro_Grupo(string filtro)
        {
            string consulta = "select * from Maestros_Grupos";
            DataSet ds = conn.ObtenerDatos(consulta, "Maestros_Grupos");
            return ds;
        }
    }
}
