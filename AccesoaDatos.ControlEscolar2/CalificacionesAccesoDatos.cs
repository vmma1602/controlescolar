﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;
namespace AccesoaDatos.ControlEscolar2
{
    public class CalificacionesAccesoDatos
    {
        ConexionAccesoaDatos conexion;
        public CalificacionesAccesoDatos()
        {
            conexion = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Entidades.ControlEscolar2.Calificaciones calificacion, int x)
        {
            if (x == 0)
            {
                //insertar'
                string consulta = "insert into calificaciones values(null," + calificacion.FkAlumno + ",'" + calificacion.FkGrupo + "'," + calificacion.FkMateria + "," + calificacion.Parcial_1 + ", "+ calificacion.Parcial_2 + "," + calificacion.Parcial_3 + "," + calificacion.Parcial_4 + ")";
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = "update calificaciones set parcial_1=" + calificacion.Parcial_1 + ",parcial_2=" + calificacion.Parcial_2 + ",parcial_3=" + calificacion.Parcial_3 + ", parcial_4 =" + calificacion.Parcial_4 + " where IDCalificacion =" + x + "";
                conexion.EjecutarConsulta(consulta);
            }


        }
        public void Eliminar(int IDCalificacion)
        {
            string consulta = "delete from calificaciones where IDCalificacion = " + IDCalificacion + " ";
            conexion.EjecutarConsulta(consulta);

        }
        public DataSet IDGrupo(string nombreGrupo)
        {
            DataSet ds;
            string consulta = "select IDGrupo from grupo where nombreGrupo like '" + nombreGrupo + "'";
            ds = conexion.ObtenerDatos(consulta, "grupo");
            return ds;
        }

        public DataSet IDMateria(string nombreMateria)
        {
            DataSet ds;
            string consulta = "select IDMateriaAuto from materias where nombreMateria like '" + nombreMateria + "'";
            ds = conexion.ObtenerDatos(consulta, "materias");
            return ds;
        }
        public DataSet IDAlumno(int NumControl)
        {
            DataSet ds;
            string consulta = "select ID, nombre,app,apm from alumno where NumControl = " + NumControl + "";
            ds = conexion.ObtenerDatos(consulta, "alumno");
            return ds;
        }
        public DataSet CargarCombos(string grupo)
        {
            DataSet ds;
            string consulta = "call LlenarCombosCalificaciones('"+grupo+"')";
            ds = conexion.ObtenerDatos(consulta, "maestros_grupos");
            return ds;
        }
        public DataSet LLenarGridCalificaciones(int fkAlumno)
        {
            DataSet ds;
            string consulta = "call LlamarBoleta("+fkAlumno+")";
            ds = conexion.ObtenerDatos(consulta, "calificaciones");
            return ds;
        }
        public DataSet LLenarComboGrupos()
        {
            DataSet ds;
            string consulta = "select distinct claveGrupo from maestros_grupos";
            ds = conexion.ObtenerDatos(consulta, "maestros_grupos");
            return ds;
        }
        public DataSet ComprobarMateriaenAlumno(int fkAlumno, int fkMateria)
        {
            DataSet ds;
            string consulta = "select * from calificaciones where fkAlumno = "+ fkAlumno + " and fkMateria ="+ fkMateria + "";
            ds = conexion.ObtenerDatos(consulta, "maestros_grupos");
            return ds;
        }
    }
}
