﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;

namespace AccesoaDatos.ControlEscolar2
{
    
    public class UsuarioAccesoDatos
    {
        ConexionAccesoaDatos conexion;
        public UsuarioAccesoDatos()
        {
            conexion = new ConexionAccesoaDatos("localhost", "root","", "control", 3306);
        }
        public void Guardar(Usuario usuario)
        {
            
            if (usuario.Idusuario==0)
            {
                //insertar
                 string consulta= string.Format("insert into usuario values(null,'{0}','{1}','{2}','{3}')",usuario.Nombre,usuario.App,usuario.Apm,usuario.Contrasenia);
                 conexion.EjecutarConsulta(consulta);
               
               
            }
            else
            {
                //update
                string consulta = string.Format("update usuario set nombre='{0}',app='{1}',apm='{2}',contrasenia='{3}' where IDusuario={4}", usuario.Nombre, usuario.App, usuario.Apm, usuario.Contrasenia,usuario.Idusuario);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idusuario)
        {
            //eliminar
            string consulta = "delete from usuario where IDusuario=" + idusuario + "";
            conexion.EjecutarConsulta(consulta);
          
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            var listUsuario = new List<Usuario>();
            var ds = new DataSet();
            string consulta = "select * from usuario where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "usuario");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
               var usuario = new Usuario
                {
                    Idusuario = Convert.ToInt32(row["IDusuario"]),
                   Nombre= row["nombre"].ToString(),
                   Apm = row["apm"].ToString(),
                   App = row["app"].ToString(),
                   Contrasenia = row["contrasenia"].ToString()

               };
                listUsuario.Add(usuario);
            }

           
           
          
           
            //Llenar lista
            return listUsuario;
        }
    }
}


