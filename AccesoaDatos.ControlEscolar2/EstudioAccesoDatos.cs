﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using System.Data;

namespace AccesoaDatos.ControlEscolar2
{
    public class EstudioAccesoDatos
    {      
            ConexionAccesoaDatos conn;
            public EstudioAccesoDatos()
            {
                conn = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
            }
            public void Guardar(Entidades.ControlEscolar2.Estudios estudio, string x)
            {

                if (x=="0")
                {
                    //inserta
                    string consulta = "insert into estudios values (null, '"+estudio.ID+"','"+estudio.Nombre+"','"+estudio.Area+"','"+estudio.Periodo+ "','" + estudio.Documento + "'," + estudio.FkMaestro + ")";
                      conn.EjecutarConsulta(consulta);
                }
                else
                {
                    //update
                    string consulta = "update  estudios set  nombre='" + estudio.Nombre + "',documento='" + estudio.Documento + "',area='" + estudio.Area + "' where IDAuto= " + x +"";
                          conn.EjecutarConsulta(consulta);
                }
            }
            public void Eliminar(int IDAuto)
            {
                //elimina
                string consulta = "delete from estudios where IDAuto=" + IDAuto + "";
                conn.EjecutarConsulta(consulta);

            }
        public void EliminarEnMaestro(string NumControl)
        {
            //elimina
            string consulta = "delete from estudios where IDestudios='" + NumControl + "'";
            conn.EjecutarConsulta(consulta);

        }
        public DataSet IDMaestro(string filtro)
        {
            var ds = new DataSet();
           string consulta = "select ID from maestros where NumControl like '"+filtro+"'";
            ds = conn.ObtenerDatos(consulta, "maestros");
            return ds;

        }
        public List<Entidades.ControlEscolar2.Estudios> GetEstudios()
        {
            var listEstudios = new List<Entidades.ControlEscolar2.Estudios>();
            var ds = new DataSet();
            string consulta = "select * from e_vista";
            ds = conn.ObtenerDatos(consulta, "e_vista");
            var dt = new DataTable();
            dt = ds.Tables[0];
          
            foreach (DataRow row in dt.Rows)
            {

                var estudios = new Entidades.ControlEscolar2.Estudios
                {   PK=Convert.ToInt32(row["IDAuto"]),                         
                    ID=row["IDestudios"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    NombreM=row["Nombre maestro"].ToString(),
                    ApellidoPaterno=row["app"].ToString(),
                    Apellidomaterno=row["apm"].ToString(),
                    Area=row["area"].ToString(),
                    Periodo =row["periodo"].ToString(),
                    Documento = row["documento"].ToString(),
                    FkMaestro =Convert.ToInt32(row["fkMaestro"])



                };
                listEstudios.Add(estudios);
            }
            //Llenar lista
            return listEstudios;
        }

    }
    }

