﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class MaestroAccesoDatos
    {
        ConexionAccesoaDatos conn;
        public  MaestroAccesoDatos()
        {
            conn = new ConexionAccesoaDatos("localhost", "root", "", "control", 3306);
        }
        public void Guardar(Entidades.ControlEscolar2.Maestros maestro,string x)
        {

            if (x=="0")
            {
                //insertar
                string consulta = "insert into maestros values(null,'" + maestro.NumeroControl + "','" + maestro.Nombre + "','" + maestro.App + "','" + maestro.Apm + "','" + maestro.Direccion + "','" + maestro.Estado + "','" + maestro.Ciudad + "','"+maestro.FechNacimiento+"','" + maestro.FechaIngreso + "'," + maestro.Cedula + ",'" + maestro.Titulo + "','"+maestro.Estudios+"')";
                conn.EjecutarConsulta(consulta);


            }
            else
            {
                //update
                string consulta = "update  maestros set  nombre='" + maestro.Nombre + "',app='" + maestro.App + "',apm='" + maestro.Apm + "',direccion='" + maestro.Direccion + "',cedula=" + maestro.Cedula + ",titulo='" + maestro.Titulo + "', fechaIngreso='" + maestro.FechaIngreso + "',fechaNacimiento='" + maestro.FechNacimiento + "',estudios='" + maestro.Estudios+"', estado='" + maestro.Estado + "', ciudad='" + maestro.Ciudad + "' where NumControl='" + maestro.NumeroControl + "'";
                conn.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(string NumControl)
        {
            //eliminar
            string a = "delete from estudios where IDestudios = '" + NumControl + "'";
            conn.EjecutarConsulta(a);
            string consulta = "delete from maestros where NumControl='" +NumControl+ "'";
            conn.EjecutarConsulta(consulta);

        }
       public DataSet MaxID(string filtro)
        {
            DataSet ds;
             string consulta= "select NumControl from maestros where NumControl like '%" + filtro + "%' ORDER BY ID DESC";
            //string consulta = "select LAST_INSERT_ID(ID) from maestros where NumControl like '%"+filtro+"%' order by ID desc";
            ds = conn.ObtenerDatos(consulta,"maestros");
            return ds;
        }
        public List<Entidades.ControlEscolar2.Maestros> GetMaestros(string filtro)
        {
            var listMaestros = new List<Entidades.ControlEscolar2.Maestros>();
            var ds = new DataSet();
             string consulta = "select * from maestros";
            ds = conn.ObtenerDatos(consulta, "maestros");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {

                var maestro = new Entidades.ControlEscolar2.Maestros
                {
                    
                    NumeroControl = row["NumControl"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    Apm = row["Apm"].ToString(),
                    App = row["App"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Estado = row["estado"].ToString(),
                    Ciudad = row["ciudad"].ToString(),
                    FechaIngreso = row["FechaIngreso"].ToString(),
                    FechNacimiento=row["FechaNacimiento"].ToString(),
                    Cedula = Convert.ToInt32(row["cedula"]),
                    Titulo = row["titulo"].ToString(),
                  

                };
                listMaestros.Add(maestro);
            }
            //Llenar lista
            return listMaestros;
        }
      public DataSet NumControlMaestro()
        {
            var ds = conn.ObtenerDatos("select * from maestros", "maestros");
            return ds;
        }
      

    }
}
