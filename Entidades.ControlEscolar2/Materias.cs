﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Materias
    {
        private string _IDMateria;
        private string _nombreMateria;
        private int _horasPractica;
        private int _horasTeoria;

        public string IDMateria { get => _IDMateria; set => _IDMateria = value; }
        public string NombreMateria { get => _nombreMateria; set => _nombreMateria = value; }
        public int HorasPractica { get => _horasPractica; set => _horasPractica = value; }
        public int HorasTeoria { get => _horasTeoria; set => _horasTeoria = value; }
    }
}
