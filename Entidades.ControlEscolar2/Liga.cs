﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
   public class Liga
    {
        private string _IDMateria;
        private string _primera;
        private string _tercera;

        public string Primera { get => _primera; set => _primera = value; }
        public string IDMateria { get => _IDMateria; set => _IDMateria = value; }
        public string Tercera { get => _tercera; set => _tercera = value; }
    }
}
