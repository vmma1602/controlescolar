﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
   public class Escuela
    {
        private int _IDEscuela;
        private string _nombreEscuela;
        private string _rfc;
        private string _domicilio;
        private string _numTel;
        private string _email;
        private string _pagWeb;
        private string _director;
        private string _logoNombre;

        public int IDEscuela { get => _IDEscuela; set => _IDEscuela = value; }
        public string NombreEscuela { get => _nombreEscuela; set => _nombreEscuela = value; }
        public string Rfc { get => _rfc; set => _rfc = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
        public string NumTel { get => _numTel; set => _numTel = value; }
        public string Email { get => _email; set => _email = value; }
        public string PagWeb { get => _pagWeb; set => _pagWeb = value; }
        public string Director { get => _director; set => _director = value; }
        public string LogoNombre { get => _logoNombre; set => _logoNombre = value; }
       
    }
}
