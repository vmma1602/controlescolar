﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
   public class Maestros
    {

       // private int _ID;
        private string _NumeroControl;
        private string _nombre;
        private string _app;
        private string _apm;
        private string _direccion;
        private string _estado;
        private string _ciudad;
        private Int32 _cedula;
        private string _fechaNacimiento;
        private string _fechaIngreso;
        private string _titulo;
        private string _estudios;
       

        public string NumeroControl { get => _NumeroControl; set => _NumeroControl = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string App { get => _app; set => _app = value; }
        public string Apm { get => _apm; set => _apm = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public string Ciudad { get => _ciudad; set => _ciudad = value; }
        public int Cedula { get => _cedula; set => _cedula = value; }
        public string FechNacimiento { get => _fechaNacimiento; set => _fechaNacimiento = value; }
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string Estudios { get => _estudios; set => _estudios = value; }
      //  public int ID { get => _ID; set => _ID = value; }
        public string FechaIngreso { get => _fechaIngreso; set => _fechaIngreso = value; }
    }
}
