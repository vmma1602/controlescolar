﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Alumnos
    {
        private int _ID;
        private int _NumControl;
        private string _nombre;
        private string _app;
        private string _apm;
        private string _domicilio;
        private string _email;
        private string _fecha;
        private string _sexo;
        private string _estado;
        private string _ciudad;
      
        public int ID { get => _ID; set => _ID = value; }
        public int NumControl { get => _NumControl; set => _NumControl = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string App { get => _app; set => _app = value; }
        public string Apm { get => _apm; set => _apm = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
        public string Email { get => _email; set => _email = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string Fecha { get => _fecha; set => _fecha = value; }     
        public string Estado { get => _estado; set => _estado = value; }
        public string Ciudad { get => _ciudad; set => _ciudad = value; }
        
    }
}
