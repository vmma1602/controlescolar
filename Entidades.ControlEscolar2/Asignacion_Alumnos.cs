﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Asignacion_Alumnos
    {
        
       
        private string _nombreGrupo;
        private int _fkGrupo;
        private int _NumControl;
        private int _fkAlumno;
        private int _asignacionID;

        public int AsignacionID { get => _asignacionID; set => _asignacionID = value; }
        public int NumControl { get => _NumControl; set => _NumControl = value; }
        public string NombreGrupo { get => _nombreGrupo; set => _nombreGrupo = value; }
        public int FkGrupo { get => _fkGrupo; set => _fkGrupo = value; }
        public int FkAlumno { get => _fkAlumno; set => _fkAlumno = value; }
        
       
    }
}
