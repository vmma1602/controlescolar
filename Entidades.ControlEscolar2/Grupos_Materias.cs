﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Grupos_Materias
    {
        private int _IDMaterias_Grupos;
        private int _fkMateria;
        private int _fkGrupo;

        public int IDMaterias_Grupos { get => _IDMaterias_Grupos; set => _IDMaterias_Grupos = value; }
        public int FkMateria { get => _fkMateria; set => _fkMateria = value; }
        public int FkGrupo { get => _fkGrupo; set => _fkGrupo = value; }
    }
}
