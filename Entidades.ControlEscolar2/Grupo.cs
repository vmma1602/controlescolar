﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Grupo
    {
        private int _IDGrupo;
        private string _nombreGrupo;
        private string _Turno;
        private int _semestre;
        private string _carrera;

        public int IDGrupo { get => _IDGrupo; set => _IDGrupo = value; }
        public string NombreGrupo { get => _nombreGrupo; set => _nombreGrupo = value; }
        public string Turno { get => _Turno; set => _Turno = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
        public int Semestre { get => _semestre; set => _semestre = value; }
       
    }
}
