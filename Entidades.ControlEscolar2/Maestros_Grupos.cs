﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Maestros_Grupos
    {
        private int _IDMaestros_Grupos;
        private string _claveMaestro;      
        private string _claveGrupo;
        private string _claveMateria;

        public int IDMaestros_Grupos { get => _IDMaestros_Grupos; set => _IDMaestros_Grupos = value; }
        public string ClaveMaestro { get => _claveMaestro; set => _claveMaestro = value; }
        public string ClaveGrupo { get => _claveGrupo; set => _claveGrupo = value; }
        public string ClaveMateria { get => _claveMateria; set => _claveMateria = value; }
    }
}
