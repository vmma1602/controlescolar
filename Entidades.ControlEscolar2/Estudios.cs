﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Estudios
    {
        private int _PK;
        private string _ID;
        private string _nombre;
        private string _periodo;
        private string _documento;
        private string _area;
        private int _fkMaestro;
        private string _NombreM;
        private string _ApellidoPaterno;
        private string _Apellidomaterno;


        public int PK { get => _PK; set => _PK = value; }
        public string ID { get => _ID; set => _ID = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string NombreM { get => _NombreM; set => _NombreM = value; }
        public string ApellidoPaterno { get => _ApellidoPaterno; set => _ApellidoPaterno = value; }
        public string Apellidomaterno { get => _Apellidomaterno; set => _Apellidomaterno = value; }
        public string Periodo { get => _periodo; set => _periodo = value; }
        public string Documento { get => _documento; set => _documento = value; }      
        public string Area { get => _area; set => _area = value; }
        public int FkMaestro { get => _fkMaestro; set => _fkMaestro = value; }
       
    }
}
