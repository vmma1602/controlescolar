﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
   public class Municipios
    {
        private string _CodigoMun;
        private string _nombreMun;
        private int _IDMun;

        public string CodigoMun { get => _CodigoMun; set => _CodigoMun = value; }
        public string NombreMun { get => _nombreMun; set => _nombreMun = value; }
        public int IDMun { get => _IDMun; set => _IDMun = value; }
    }
}
