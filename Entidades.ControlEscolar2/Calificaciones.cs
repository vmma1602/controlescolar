﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Calificaciones
    {
        private int _IDCalificacion;
        private int _fkAlumno;
        private int _fkGrupo;
        private int _fkMateria;
        private int _Parcial_1;
        private int _Parcial_2;
        private int _Parcial_3;
        private int _Parcial_4;

        public int IDCalificacion { get => _IDCalificacion; set => _IDCalificacion = value; }
        public int FkAlumno { get => _fkAlumno; set => _fkAlumno = value; }
        public int FkGrupo { get => _fkGrupo; set => _fkGrupo = value; }
        public int FkMateria { get => _fkMateria; set => _fkMateria = value; }
        public int Parcial_1 { get => _Parcial_1; set => _Parcial_1 = value; }
        public int Parcial_2 { get => _Parcial_2; set => _Parcial_2 = value; }
        public int Parcial_3 { get => _Parcial_3; set => _Parcial_3 = value; }
        public int Parcial_4 { get => _Parcial_4; set => _Parcial_4 = value; }
    }
}
