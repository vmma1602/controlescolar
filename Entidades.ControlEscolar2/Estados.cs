﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
   public class Estados
    {
        private string _CodigoEstado;
        private string nombre;

        public string CodigoEstado { get => _CodigoEstado; set => _CodigoEstado = value; }
        public string Nombre { get => nombre; set => nombre = value; }
    }
}
