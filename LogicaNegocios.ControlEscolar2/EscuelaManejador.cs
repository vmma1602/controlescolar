﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class EscuelaManejador
    {
        EscuelaAccesoDatos _escuelaAccesoDatos = new EscuelaAccesoDatos();
        public List<Entidades.ControlEscolar2.Escuela> GetEscuela(string filtro)
        {
            var ListEscuela = _escuelaAccesoDatos.GetEscuela(filtro);
            return ListEscuela;
        }
        public DataSet IDUnico(string filtro)
        {
            DataSet ds;
            ds = _escuelaAccesoDatos.IDUnico(filtro);
            return ds;
        }
        public void Guardar(Entidades.ControlEscolar2.Escuela escuela, int x)
        {
            
            _escuelaAccesoDatos.Guardar(escuela, x);
        }
        public void Eliminar()
        {
            _escuelaAccesoDatos.Eliminar();
        }
    }
}
