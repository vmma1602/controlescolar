﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
namespace LogicaNegocios.ControlEscolar2
{
    public class GrupoManejador
    {
        GrupoAccesoDatos _grupoAccesoDatos = new GrupoAccesoDatos();
        public void Guardar(Entidades.ControlEscolar2.Grupo grupo, int x)
        {
            _grupoAccesoDatos.Guardar(grupo, x);

        }
        public void Eliminar(int IDGrupo)
        {
            _grupoAccesoDatos.Eliminar(IDGrupo);

        }
        public List<Entidades.ControlEscolar2.Grupo> GetGrupo(string filtro)
        {
            var ListGrupo = new List<Entidades.ControlEscolar2.Grupo>();
            ListGrupo = _grupoAccesoDatos.GetGrupo(filtro);
            return ListGrupo;
        }
        public DataSet IDGrupoDS()
        {
            DataSet ds;
            ds = _grupoAccesoDatos.IDGrupoDS();
            return ds;
        }
    }
}
