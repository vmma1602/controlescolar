﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;
namespace LogicaNegocios.ControlEscolar2
{
    public class MateriaManejador
    {
        MateriasAccesoDatos _materiasAccesoDatos = new MateriasAccesoDatos();
        public void Guardar(Entidades.ControlEscolar2.Materias materia, string x, int a)
        {
            _materiasAccesoDatos.Guardar(materia, x, a);
        }
        public List<Materias> GetMaterias(string filtro)
        {
            var listMaterias = new List<Materias>();
            listMaterias = _materiasAccesoDatos.GetMaterias(filtro);
            return listMaterias;
        }
        public DataSet MaxIDMateria(string filtro)
        {
            DataSet ds;
            ds = _materiasAccesoDatos.MaxID(filtro);
            return ds;
        }
        public void Eliminar(string IDMateria)
        {
            _materiasAccesoDatos.Eliminar(IDMateria);


        }
        public DataSet IDAutoincrementable(string filtro)
        {
            DataSet ds = _materiasAccesoDatos.IDAutoincrementable(filtro);
            return ds;
        }

    }
}
