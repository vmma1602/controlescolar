﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class Grupos_MateriasManejador
    {
        Materias_GruposAccesoDatos _materias_GruposAccesoDatos = new Materias_GruposAccesoDatos();
        public void Guardar(Entidades.ControlEscolar2.Grupos_Materias grupo_materia, int x)
        {
            _materias_GruposAccesoDatos.Guardar(grupo_materia, x);
        }
        public void Eliminar(int IDMateria_Grupo)
        {
            _materias_GruposAccesoDatos.Eliminar(IDMateria_Grupo);
        }
        public List<Entidades.ControlEscolar2.Grupos_Materias> GetGrupos_Materias()
        {
            var ListGrupo = _materias_GruposAccesoDatos.GetGrupos_Materias();
            return ListGrupo;
        }
        public DataSet Grupos_MateriasDS()
        {
            DataSet ds = _materias_GruposAccesoDatos.Grupos_MateriasDS();           
            return ds;
        }
    }
}
