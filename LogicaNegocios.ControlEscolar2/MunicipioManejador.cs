﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class MunicipioManejador
    {
        private CiudadAccesoDatos _ciudadAccesoDatos = new CiudadAccesoDatos();
        public List<Municipios> GetCiudadesList(string x)
        {
            var listMunicipios = new List<Municipios>();
            listMunicipios = _ciudadAccesoDatos.GetciudadesList(x);
            return listMunicipios;
        }
    }
}
