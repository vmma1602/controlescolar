﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class LigaManejador
    {
        LigaAccesoDatos _ligaAccesoDatos = new LigaAccesoDatos();
        public void Guardar(Entidades.ControlEscolar2.Liga liga)
        {
            _ligaAccesoDatos.Guardar(liga);
        }
        public List<Liga> GetLiga(string filtro)
        {
            var listLiga = new List<Liga>();
            listLiga = _ligaAccesoDatos.GetLiga(filtro);
            return listLiga;
        }
        public DataSet ObtenerTodo()
        {
            DataSet ds = _ligaAccesoDatos.ObtenerTodo();           
            return ds;
        }



        public void ActualizarPrimero(Entidades.ControlEscolar2.Liga liga, int filtro)
        {
            _ligaAccesoDatos.ActualizarPrimero(liga, filtro);
        }
        public void ActualizarSegundo(Entidades.ControlEscolar2.Liga liga, int filtro)
        {
            _ligaAccesoDatos.ActualizarSegunda(liga, filtro);
        }
        public void ActualizarTercero(Entidades.ControlEscolar2.Liga liga, int filtro)
        {
            _ligaAccesoDatos.ActualizarTercera(liga, filtro);
        }
        public DataSet dsLiga(string filtro)
        {
            DataSet ds;
            ds = _ligaAccesoDatos.dsLiga(filtro);
            return ds;
        }
        public void Eliminar(int IDMateria)
        {
            _ligaAccesoDatos.Eliminar(IDMateria);
        }
    }
}
