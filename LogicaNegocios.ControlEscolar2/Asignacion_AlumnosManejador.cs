﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
namespace LogicaNegocios.ControlEscolar2
{
    public class Asignacion_AlumnosManejador
    {
        AlumnoAccesoDatos _alumnoAccesoDatos = new AlumnoAccesoDatos();
        Asignacion_AlumnosAccesoDatos _asignacion_Alumnos = new Asignacion_AlumnosAccesoDatos();

        public void Guardar(Entidades.ControlEscolar2.Asignacion_Alumnos asignacion, int x)
        {
            _asignacion_Alumnos.Guardar(asignacion, x);
        }
        public void Eliminar(int IDAsignacion)
        {
            _asignacion_Alumnos.Eliminar(IDAsignacion);

        }
        
        public List<Entidades.ControlEscolar2.Asignacion_Alumnos> GetAsignacion_Alumnos(string filtro)
        {
            var ListAsignacion = new List<Entidades.ControlEscolar2.Asignacion_Alumnos>();
            ListAsignacion = _asignacion_Alumnos.GetAsignacion_Alumnos(filtro);
            return ListAsignacion;
        }
        public DataSet TodoAlumnos(int x)
        {
            DataSet ds = _asignacion_Alumnos.TodoAlumnos(x);           
            return ds;
        }
        public DataSet nombreGrupoDS(string nombreGrupo)
        {
            DataSet ds = _asignacion_Alumnos.nombreGrupoDS(nombreGrupo);         
            return ds;
        }
        public DataSet ComprobarAlummnoenGrupo(int NumControl)
        {
            DataSet ds = _asignacion_Alumnos.ComprobarAlummnoenGrupo(NumControl);
            return ds;
        }
    }
}
