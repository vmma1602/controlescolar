﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
     public class EstudioManejador
    {
        private EstudioAccesoDatos _estudioAccesoDatos = new EstudioAccesoDatos();
        public void Guardar(Entidades.ControlEscolar2.Estudios estudios, string x)
        {

            _estudioAccesoDatos.Guardar(estudios,x);

        }
        public DataSet IDMaestro(string filtro)
        {
            var ds = new DataSet();
            ds = _estudioAccesoDatos.IDMaestro(filtro);
            return ds;

        }
        public void Eliminar(int IDAuto)
        {
            _estudioAccesoDatos.Eliminar(IDAuto);

        }
        public void EliminarEnMaestro(string NumControl)
        {
            _estudioAccesoDatos.EliminarEnMaestro(NumControl);

        }
        public List<Entidades.ControlEscolar2.Estudios> GetEstudios(string filtro)
        {
            var listEstudios = new List<Entidades.ControlEscolar2.Estudios>();
            listEstudios = _estudioAccesoDatos.GetEstudios();
            //Llenar lista
            return listEstudios;
        }

    }
}
