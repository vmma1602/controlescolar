﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
   public class CalificacionesManejador
    {
        CalificacionesAccesoDatos _calificacionesAccesoDatos = new CalificacionesAccesoDatos();
        public void Guardar(Entidades.ControlEscolar2.Calificaciones calificacion, int x)
        {
            _calificacionesAccesoDatos.Guardar(calificacion, x);
        }
        public void Eliminar(int IDCalificacion)
        {
            _calificacionesAccesoDatos.Eliminar(IDCalificacion);

        }
        public DataSet IDMateria(string nombreMateria)
        {
            DataSet ds = _calificacionesAccesoDatos.IDMateria(nombreMateria);         
            return ds;
        }
        public DataSet IDAlumno(int NumControl)
        {
            DataSet ds  = _calificacionesAccesoDatos.IDAlumno(NumControl);      
            return ds;
        }
        public DataSet CargarCombos(string grupo)
        {
            DataSet ds = _calificacionesAccesoDatos.CargarCombos(grupo);          
            return ds;
        }
        public DataSet LLenarGridCalificaciones(int fkAlumno)
        {
            DataSet ds = _calificacionesAccesoDatos.LLenarGridCalificaciones(fkAlumno);          
            return ds;
        }
        public DataSet LLenarComboGrupos()
        {
            DataSet ds = _calificacionesAccesoDatos.LLenarComboGrupos();
            return ds;
        }
        public DataSet IDGrupo(string nombreGrupo)
        {
            DataSet ds = _calificacionesAccesoDatos.IDGrupo(nombreGrupo);          
            return ds;
        }
        public DataSet ComprobarMateriaenAlumno(int fkAlumno, int fkMateria)
        {
            DataSet ds = _calificacionesAccesoDatos.ComprobarMateriaenAlumno(fkAlumno, fkMateria);       
            return ds;
        }
    }
}
