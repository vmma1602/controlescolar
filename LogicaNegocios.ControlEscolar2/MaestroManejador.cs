﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class MaestroManejador
    {
        private MaestroAccesoDatos _MaestroAccesoDatos = new MaestroAccesoDatos();
        public void Guardar(Entidades.ControlEscolar2.Maestros maestro,string x)
        {
            _MaestroAccesoDatos.Guardar(maestro,x);
        }
        public void Eliminar(string NumControl)
        {
            _MaestroAccesoDatos.Eliminar(NumControl);

        }
       
        public List<Entidades.ControlEscolar2.Maestros> GetMaestros(string filtro)
        {
            var listMaestro = _MaestroAccesoDatos.GetMaestros(filtro);
            //Llenar lista
            return listMaestro;
        }
        public DataSet MaxID(string filtro)
        {
            DataSet ds;
            ds = _MaestroAccesoDatos.MaxID(filtro);
            return ds;
        
        }
        public DataSet NumControlMaestro()
        {
            var ds = _MaestroAccesoDatos.NumControlMaestro();
            return ds;
        }

    }
}
