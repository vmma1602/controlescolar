﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.Data;
namespace LogicaNegocios.ControlEscolar2
{
    public class AlumnoManejador
    {
        private AlumnoAccesoDatos _alumnoAccesoDatos = new AlumnoAccesoDatos();
        public void Guardar(Alumnos alumno)
        {
            _alumnoAccesoDatos.Guardar(alumno);

        }
        public void Eliminar(int NumControl)
        {
            //eliminar
            _alumnoAccesoDatos.Eliminar(NumControl);
        }
        public List<Alumnos> GetAlumnos(string filtro)
        {          
           var listAlumnos = _alumnoAccesoDatos.GetAlumnos(filtro);
            //Llenar lista
            return listAlumnos;
        }
       
       
       
      
       
    }
}
