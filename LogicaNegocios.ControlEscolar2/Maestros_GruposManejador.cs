﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;
namespace LogicaNegocios.ControlEscolar2
{
    public class Maestros_GruposManejador
    {
        Maestros_GruposAccesoDatos _maestros_GruposAccesoDatos = new Maestros_GruposAccesoDatos();
        public void Guardar(Entidades.ControlEscolar2.Maestros_Grupos maestro_grupo, int x)
        {
            _maestros_GruposAccesoDatos.Guardar(maestro_grupo, x);
        }
        public void Eliminar(int IDMAestro_Grupo)
        {
            _maestros_GruposAccesoDatos.Eliminar(IDMAestro_Grupo);
        }

        public List<Entidades.ControlEscolar2.Maestros_Grupos> GetMaestros_Grupos(string filtro)
        {
            var listMaestros_Grupos = new List<Entidades.ControlEscolar2.Maestros_Grupos>();
            listMaestros_Grupos = _maestros_GruposAccesoDatos.GetMaestros_Grupos(filtro);
            return listMaestros_Grupos;
        }
        public DataSet IDMaestro_Grupo(string filtro)
        {

            DataSet ds = _maestros_GruposAccesoDatos.IDMaestro_Grupo(filtro);
            return ds;
        }
    }
}
