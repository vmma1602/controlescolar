﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class EstadoManejador
    {
        private EstadosAccesoDatos _estadoAccesoDatos = new EstadosAccesoDatos();
        public List<Estados> GetEstadosList()
        {          
                var listEstados = new List<Estados>();
                listEstados = _estadoAccesoDatos.GetEstadosList();
                return listEstados;
            
        }
        public List<Estados> CodigoEstado(string x)
        {
            var listEstados = new List<Estados>();
            listEstados = _estadoAccesoDatos.CodigoEstado(x);
            return listEstados;

        }


    }
}
