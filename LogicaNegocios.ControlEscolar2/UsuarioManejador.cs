﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class UsuarioManejador
    {
        private UsuarioAccesoDatos _usuarioAccesoDatos = new UsuarioAccesoDatos();
        public void Guardar(Usuario usuario)
        {
            _usuarioAccesoDatos.Guardar(usuario);

        }
        public void Eliminar(int idusuario)
        {
            //eliminar
            _usuarioAccesoDatos.Eliminar(idusuario);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listUsuario = _usuarioAccesoDatos.GetUsuarios(filtro);
            //Llenar lista
            return listUsuario;
        }

    }
}
